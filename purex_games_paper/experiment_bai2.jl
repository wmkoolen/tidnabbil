# replicate the second experiment from Optimal Best Arm Identification
# with Fixed Confidence (Garivier and Kaufmann 2016).
#
# running on my laptop this takes
# 437.947061 seconds (9.65 M allocations: 1.015 GiB, 0.06% gc time)

using JLD2;
using Distributed;
using Printf;

@everywhere include("runit.jl");
@everywhere include("../thresholds.jl");
include("../experiment_helpers.jl");

# setup

dist = Bernoulli();
μ = [.3, .21, .2, .19, .18];
pep = BestArm(dist);

srs = everybody(pep, μ);


δs = (0.1, 0.05, 0.01, 0.001, 0.0001);
βs = GK16.(δs); # Recommended in section 6 of paper

N = 3000;
seed = 1234;


# compute

@time data = pmap(
    ((sr,i),) -> runit(seed+i, sr, μ, pep, βs),
    Iterators.product(srs, 1:N)
);

dump_stats(pep, μ, δs, βs, srs, data);


# save

@save isempty(ARGS) ? "experiment_bai2.dat" : ARGS[1]  dist μ pep srs data δs βs N seed

# visualise by loading viz_bai2.jl
