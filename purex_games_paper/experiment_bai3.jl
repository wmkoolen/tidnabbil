# replicate the experiment from Gradient Ascent for Active Explorationin Bandit Problems (Menard 2019).
#
# Note that Menard's optimal weights are imprecise. The actual answer is
# w^* = 0.4125  0.3792  0.1521  0.0561
# which is confirmed by the current library and by the independent CVX optimiser
#
# running on my laptop this takes
# 971.435398 seconds (10.35 M allocations: 1.117 GiB, 0.03% gc time)

using JLD2;
using Distributed;
using Printf;

@everywhere include("runit.jl");
@everywhere include("../thresholds.jl");
include("../experiment_helpers.jl");

# setup

dist = Gaussian();
μ = [1., 0.85, 0.8, 0.7];
pep = BestArm(dist);

srs = everybody(pep, μ);


δs = (0.1, 0.05, 0.01, 0.001, 0.0001);
βs = GK16.(δs); # Recommended in section 4 of paper

N = 1000;
seed = 1234;


# compute

@time data = pmap(
    ((sr,i),) -> runit(seed+i, sr, μ, pep, βs),
    Iterators.product(srs, 1:N)
);

dump_stats(pep, μ, δs, βs, srs, data);


# save

@save isempty(ARGS) ? "experiment_bai3.dat" : ARGS[1]  dist μ pep srs data δs βs N seed

# visualise by loading viz_bai1.jl
