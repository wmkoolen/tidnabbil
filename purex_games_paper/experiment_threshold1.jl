# replicate the first experiment from Sequential Test for the Lowest
# Mean: From Thompson to Murphy Sampling (Kaufmann and Koolen 2018).
#
# running on my laptop this takes
# TODO

using JLD2;
using Distributed;
using Printf;

@everywhere include("runit.jl");
@everywhere include("../thresholds.jl");
include("../experiment_helpers.jl");

# setup

dist = Gaussian();
μ = collect(range(-1, stop=+1, length=10));
γ = 0;
pep = MinimumThreshold(dist, γ);

srs = everybody(pep, μ);


δs = (0.1, 0.05, 0.01, 0.001, 0.0001, exp(-23));
βs = GK16.(δs); # NOT the stopping rule from the paper

N = 5000;
seed = 1234;


# compute

@time data = pmap(
    ((sr,i),) -> runit(seed+i, sr, μ, pep, βs),
    Iterators.product(srs, 1:N)
);

dump_stats(pep, μ, δs, βs, srs, data);


# save

@save isempty(ARGS) ? "experiment_threshold1.dat" : ARGS[1]  dist μ pep srs data δs βs N seed

# visualise by loading viz_threshold1.jl
