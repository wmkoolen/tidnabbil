# "Hard" Threshold problem on which Track-and-Stop underwhelms
#
# running on my laptop this takes
# TODO

using JLD2;
using Distributed;
using Printf;

@everywhere include("runit.jl");
@everywhere include("../thresholds.jl");
include("../experiment_helpers.jl");

# setup

dist = Gaussian();
μ = [.5, .6]; # parameters
γ = .6;
pep = MinimumThreshold(dist, γ);

srs = everybody(pep, μ);


δs = (0.1, 0.05, 0.01, 0.001, 0.0001, 1e-10, 1e-20);
βs = GK16.(δs);

N = 5000;
seed = 1234;


# compute

@time data = pmap(
    ((sr,i),) -> runit(seed+i, sr, μ, pep, βs),
    Iterators.product(srs, 1:N)
);

dump_stats(pep, μ, δs, βs, srs, data);


# save

@save isempty(ARGS) ? "experiment_threshold3.dat" : ARGS[1]  dist μ pep srs data δs βs N seed

# visualise by loading viz_threshold3.jl
