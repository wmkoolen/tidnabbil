function PAVAprep(N, θ)

    K = length(N);

    # cumulative sums: e.g. sum(N[a:b]) = csN[1+b]-csN[a]
    csN   = [0 cumsum(N)...];
    csNθ  = [0 cumsum(N.*θ)...];
    csNθ² = [0 cumsum(N.*θ.^2)...];

    # prv[i]+1 is the first member of the rightmost plateau in the
    # optimal increasing function on 1..i
    prv = zeros(Int64, K);

    # nxt[i]-1 is the last member of the leftmost plateau in the
    # optimal decreasing function on i..end
    nxt = zeros(Int64, K);

    # costs
    bwc = zeros(K+1); # bwc[i+1] is cost of best inc function on 1:i
    fwc = zeros(K+1); # fwc[i] is cost of best dec function on i:end

    for i in 1:K
        prv[i] = i-1; # add singleton block
        while prv[i] > 0 &&
            (csNθ[1+i]-csNθ[prv[i]+1]) * (csN[1+prv[i]]-csN[prv[prv[i]]+1])  ≤
            (csN[1+i]-csN[prv[i]+1]) * (csNθ[1+prv[i]]-csNθ[prv[prv[i]]+1])
            prv[i] = prv[prv[i]]; # merge last two blocks
        end
        bwc[1+i] = bwc[1+prv[i]] +
            (csN[1+i] == csN[1+prv[i]] ? 0. :
             .5*((csNθ²[1+i]-csNθ²[1+prv[i]]) -
                 (csNθ[1+i]-csNθ[1+prv[i]])^2/(csN[1+i]-csN[1+prv[i]])));
    end

    for i in K:-1:1
        nxt[i] = i+1; # add singleton block
        while nxt[i] ≤ K &&
            (csNθ[nxt[i]]-csNθ[i]) * (csN[nxt[nxt[i]]]-csN[nxt[i]])  ≤
            (csN[nxt[i]]-csN[i]) * (csNθ[nxt[nxt[i]]]-csNθ[nxt[i]])
            nxt[i] = nxt[nxt[i]]; # merge last two blocks
        end
        fwc[i] = fwc[nxt[i]] +
            (csN[nxt[i]] == csN[i] ? 0. :
             .5*((csNθ²[nxt[i]]-csNθ²[i]) -
                 (csNθ[nxt[i]]-csNθ[i])^2/(csN[nxt[i]]-csN[i])));
    end

    prv, nxt, bwc, fwc, csN, csNθ, csNθ²
end

function decodel!(λ, prv, nxt, bwc, fwc, csN, csNθ, csNθ², l)
    while l != 0
        λ[prv[l]+1:l] .= csN[1+l]==csN[prv[l]+1] ? 0. : (csNθ[1+l]-csNθ[prv[l]+1])/(csN[1+l]-csN[prv[l]+1]);
        l = prv[l];
    end
end

function decoder!(λ, prv, nxt, bwc, fwc, csN, csNθ, csNθ², r)
    K = length(prv);
    while r != K+1
        λ[r:nxt[r]-1] .= csN[nxt[r]]==csN[r] ? 0. : (csNθ[nxt[r]]-csNθ[r])/(csN[nxt[r]]-csN[r]);
        r = nxt[r];
    end
end

function decode(prv, nxt, bwc, fwc, csN, csNθ, csNθ², kmin)
    λ = fill(NaN, length(prv)); # allocate; entries get set below
    decodel!(λ, prv, nxt, bwc, fwc, csN, csNθ, csNθ², kmin-1);
    decoder!(λ, prv, nxt, bwc, fwc, csN, csNθ, csNθ², kmin);
    λ
end



function PAVA_project(N, θ)
    prv, nxt, bwc, fwc, csN, csNθ, csNθ² = PAVAprep(N, θ)

    # find optimal unimodal function. It is increasing on 1:kmin-1,
    # then decreasing on kmin:end
    vmin, kmin = findmin(bwc.+fwc);
    vmin, decode(prv, nxt, bwc, fwc, csN, csNθ, csNθ², kmin)
end


function PAVA_alt_min(N, θ, J)
    prv, nxt, bwc, fwc, csN, csNθ, csNθ² = PAVAprep(N, θ)
    K = length(N);

    # we are looking for a function in {¬J}, i.e. that is either
    # increasing until J+1 (or further), or
    # decreasing from J-1 (or earlier)
    v, kmin = minimum((bwc[i] .+ fwc[i], i) for i in 1:K+1 if !(J ≤ i ≤ J+1));
    v, decode(prv, nxt, bwc, fwc, csN, csNθ, csNθ², kmin)
end


function PAVA_alt_min(N, θ, J, k)
    @assert k != J
    prv, nxt, bwc, fwc, csN, csNθ, csNθ² = PAVAprep(N, θ)

    i = k < J ? k : k+1;
    bwc[i] .+ fwc[i], decode(prv, nxt, bwc, fwc, csN, csNθ, csNθ², i)
end



function PAVA_alt_min_constr(N, θ, J)
    @assert θ[J] ≥ maximum(θ)
    prv, nxt, bwc, fwc, csN, csNθ, csNθ² = PAVAprep(N, θ)
    K = length(N);

    # the original maximum θ[J] has to stay put, and somebody else has
    # to become better than J. So this guy must be J-1 or J+1

    v1 = J+1 ≤ K ? bwc[1+J]   + .5*N[J+1]*(θ[J+1]-θ[J])^2 + fwc[J+2] : Inf;
    v2 = J-1 ≥ 1 ? bwc[1+J-2] + .5*N[J-1]*(θ[J-1]-θ[J])^2 + fwc[J]   : Inf;

    λ = fill(NaN, K);
    if v1 < v2
        decodel!(λ, prv, nxt, bwc, fwc, csN, csNθ, csNθ², J);
        λ[J+1] = θ[J];
        decoder!(λ, prv, nxt, bwc, fwc, csN, csNθ, csNθ², J+2);
        v1, λ
    else
        decodel!(λ, prv, nxt, bwc, fwc, csN, csNθ, csNθ², J-2);
        λ[J-1] = θ[J];
        decoder!(λ, prv, nxt, bwc, fwc, csN, csNθ, csNθ², J);
        v2, λ
    end
end
