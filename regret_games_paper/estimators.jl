abstract type Estimator
end

function estimate_parameter(str::Structure, est::Estimator, expfam, N, µ)
	return µ
end

# TODO: What is the contract for this fuction? Why does it not compute
# the projection on the structure?
function estimate_means(str::Structure, est::Estimator, expfam, N, µ)
	return µ
end

function estimate_best_arm(str::Structure, est::Estimator, expfam, N, µ)
	return argmax(estimate_means(str, est, expfam, N, µ))
end

struct UnconstrainedEstimator <: Estimator
end

struct StructuredEstimator <: Estimator
end

function confidence_interval(str::Structure, est::Union{UnconstrainedEstimator, StructuredEstimator}, expfam, N, µ, width)
    return µ, [[ddn(expfam, µ[k], width / N[k]), dup(expfam, µ[k], width / N[k])] for k  in eachindex(µ)]
end

function confidence_interval(str::Sparse, est::StructuredEstimator, expfam, N, µ, width)
	K = length(N)
    intervals = [[ddn(expfam, µ[k], width./N[k]), dup(expfam, µ[k], width./N[k])] for k in 1:K]

    nb_big = sum(intervals[k][1] > str.γ for k in 1:K)  # count how many arms have to be > γ
    if (nb_big > str.s) || any(intervals[k][2] < str.γ for k in 1:K)  # should not happen. In that case, the concentration failed
        return µ, intervals
    end
    for k in 1:K
        if intervals[k][1] <= str.γ
            intervals[k][1] = str.γ
            if nb_big == str.s
                intervals[k][2] = str.γ
            end
        end
    end
    return µ, intervals
end

function confidence_interval(str::Simplex, est::StructuredEstimator, expfam, N, µ, width)
    intervals = [
    	[ddn(expfam, µ[k], width / N[k]), dup(expfam, µ[k], width / N[k])]
    		for k  in eachindex(µ)
    ]
    concentration_failed = (
    	   sum(min(1, intervals[i][2]) for i in eachindex(µ)) < 1
    	|| sum(max(0, intervals[i][1]) for i in eachindex(µ)) > 1
    	|| any(intervals[i][1] > 1 for i in eachindex(µ))
    	|| any(intervals[i][2] < 0 for i in eachindex(µ))
    )
    if concentration_failed || any(N .< 1)
    	return µ, intervals
    end
    intervals = [clamp.(intervals[k], 0, 1) for k in eachindex(µ)]
    for k in eachindex(µ)
    	intervals[k][1] = max(intervals[k][1], max(1-sum(intervals[i][2] for i in eachindex(µ) if i != k), 0))
    	intervals[k][2] = min(intervals[k][2], max(1-sum(intervals[i][1] for i in eachindex(µ) if i != k), 0))
    end
    return µ, intervals
end

function confidence_interval(str::Categorized, est::StructuredEstimator, expfam, N, µ, width)
    K = length(N)
    intervals = [
    	[ddn(expfam, µ[k], width / N[k]), dup(expfam, µ[k], width / N[k])]
    	for k  in eachindex(µ)
    ]

    L = 1:str.nb1;
    R = str.nb1+1:K;

    Llo = maximum(intervals[k][1] for k in L);
    Lhi = minimum(intervals[k][2] for k in L);
    Rlo = maximum(intervals[k][1] for k in R);
    Rhi = minimum(intervals[k][2] for k in R);

    cat1_is_best = Llo > Rhi
    cat2_is_best = Lhi < Rlo
    concentration_failed = (
    	cat1_is_best && cat2_is_best  # TODO: is this possible?
    )
    if concentration_failed || any(N .< 1)
    	return µ, intervals
    end
    if cat1_is_best
    	for k in L
    	    intervals[k][1] = max(intervals[k][1], Rlo)
    	end
    	for k in R
    	    intervals[k][2] = min(intervals[k][2], Lhi)
    	end
    elseif cat2_is_best
    	for k in L
    	    intervals[k][2] = min(intervals[k][2], Rhi)
    	end
    	for k in R
    	    intervals[k][1] = max(intervals[k][1], Llo)
    	end
    end
    return µ, intervals
end

function estimate_parameter(str::Linear, est::StructuredEstimator, expfam::Gaussian, N, µ)
    Gram = str.xs'*Diagonal(N)*str.xs + 0.1*I # why is regularisation correct/useful?
    inv_Gram = inv(Gram)

    vect = sum(N[k] .* µ[k] .* str.xs[k,:] for k in eachindex(N))
    return inv_Gram * vect
end

function estimate_means(str::Linear, est::StructuredEstimator, expfam::Gaussian, N, µ)
	return str.xs * estimate_parameter(str, est, expfam, N, µ)
end

function confidence_interval(str::Linear, est::StructuredEstimator, expfam::Gaussian, N, µ, width)
	intervals = [zeros(2) for k in eachindex(µ)]
    if sum(N)<length(N)
        return µ, [[ddn(expfam, µ[k], width/N[k]), dup(expfam, µ[k], width/N[k])] for k in eachindex(µ)]
    end
    K,_ = size(str.xs)
    Gram = str.xs'*Diagonal(N)*str.xs + 0.1*I # why is regularisation correct/useful?
    inv_Gram = inv(Gram)
    vect = sum(N[k] .* µ[k] .* str.xs[k,:] for k in 1:K)
    ξ = inv_Gram * vect
    means = str.xs * ξ
    confidence_width = sqrt.(2 * width *transpose(str.xs[i,:])*inv_Gram*str.xs[i,:] for i in 1:K)
    for k in 1:K
        intervals[k][1] = means[k] - confidence_width[k]
        intervals[k][2] = means[k] + confidence_width[k]
    end
    return means, intervals
end
