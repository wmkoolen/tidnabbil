using Distributed;
using JLD2;


@everywhere include("runit.jl");

# setup
K = 4
ξ = [0.5, 1]
x = [cumsum(ones(K)) ones(K)]
μ = x*ξ
str = Linear(x);
expfam = Gaussian();

@assert conforms(str, μ)


T = 10000;
nreps = 40;

str_est = StructuredEstimator()
unc_est = UnconstrainedEstimator()
algs = ((SPk(CTracking), unc_est), (SPk(CTracking), str_est), (UCB(), unc_est), (UCB(), str_est));

# compute

# only as reproducible as the optimiser :(
@time data = pmap(
    ((i, (sr, est)),) -> runit(i, sr, str, est, expfam, μ, T),
    Iterators.product(1:nreps, algs)
)

# save
@save (isempty(ARGS) ? "experiment_affine1" : ARGS[1])*".dat"  str expfam μ T nreps algs data
