using Distributed;
using JLD2;


@everywhere include("runit.jl");

# setup

µ = 2*[1., 0.5, 0.48, 0.]
str = Categorized(1);
expfam = Gaussian();

@assert conforms(str, μ)


T = 50000;
nreps = 200;

str_est = StructuredEstimator()
unc_est = UnconstrainedEstimator()
algs = ((CATSE(1/T),str_est),
        (OSSBLike(0.02/sqrt(log(T)),0), str_est),
        (SPk(CTracking), str_est),
        (SPλ(), str_est),
        (UCB(), unc_est),
        (UCB(), str_est)
        );
#(OSSBLike(0.02/sqrt(log(T)),0), str_est), (SPk(CTracking), str_est), (UCB(), unc_est), (UCB(), str_est)
# compute

# only as reproducible as the optimiser :(
@time data = pmap(
    ((i, (sr, est)),) -> runit(i, sr, str, est, expfam, μ, T),
    Iterators.product(1:nreps, algs)
)

# save
@save (isempty(ARGS) ? "experiment_categorized1" : ARGS[1])*".dat" str expfam μ T nreps algs data
