using Plots;
using Distributed;


@everywhere include("runit.jl");


ver = 5

if ver == 0
    #μ = [.52, .16, .16, .16];
    μ = [.49, .17, .17, .17];
    str = Simplex();
    expfam = Gaussian();
elseif ver == 1
    μ = collect(range(0, 1, length=4));
    str = Affine(collect(1:4));
    expfam = Gaussian();
elseif ver == 2
    μ = [.5, .8, .3, .3, .3, .3, .3, .3, .3, .3, .3];
    str = Sparse(2, .3);
    expfam = Gaussian();
elseif ver == 3
    μ = collect(range(0, 1, length=4));
    str = Unconstrained();
    expfam = Gaussian();
elseif ver == 4
    x = collect(range(0, 1, length=5));
    μ = @. .1 + .8*4*x*(1-x);
    str = Concave(x);
    expfam = Gaussian();
elseif ver == 5
    ρs = range(0, 2π, length=6);
    x = [cos.(ρs) sin.(ρs)];
    ξ = [1,2];
    μ = x*ξ; # well-specified
    str = Linear(x);
    expfam = Gaussian();
else
    @assert false
end


T = 10000;
nreps = 3;


@assert conforms(str, μ)

algs = (SPλ(), SPk(CTracking), SPk(DTracking), UCB());

# only as reproducible as the optimiser :(
@time data = pmap(
    ((i, a),) -> runit(i, a, str, expfam, μ, T),
    Iterators.product(1:nreps, algs)
)

plot(title="\\mu = $μ in $str", xlabel="T", ylabel="regret", legend=:bottomright);

for i in eachindex(algs)
    ER = sum(getindex.(data[:,i],1)) / nreps;
    σR = sqrt.(sum(map(x -> (x[1].-ER).^2, data[:,i]))/nreps^2);

    plot!(ER, ribbon=σR, label=long(algs[i]))
end

Δ = maximum(μ) .- μ
ds = d.(Ref(expfam), μ, maximum(μ));
rate = sum(Δ[k]/ds[k] for k in eachindex(μ) if Δ[k]>0)

@time _,_,v = sp_perturbed_k(str, expfam, μ, 10000, t->1/t);
rate2 = 1/v[end];

plot!([rate rate2].*log.(1:T), label=["uncstrd lbd" "approx lbd"])

gui()
