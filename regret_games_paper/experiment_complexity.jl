using Distributed;
using JLD2;


@everywhere include("runit.jl");

αs = range(.5, 6, length=40);

# setup
str = Unimodal();
expfam = Gaussian();


T = 10000;
nreps = 100;

str_est = StructuredEstimator()
unc_est = UnconstrainedEstimator()
algs = ((SPk(CTracking), unc_est),
        (SPλ(), unc_est),
        #(UCB(), unc_est),
        #(OSSBLike(0.02/sqrt(log(T)),0), unc_est),
        #(OSUB(), unc_est)
        );

μ = [.2, .4, .9, .7, .1];
@assert conforms(str, μ)


# compute

# only as reproducible as the optimiser :(
@time data = pmap(
    ((i, (sr, est), α),) -> runit(i, sr, str, est, expfam, α .* μ, T),
    Iterators.product(1:nreps, algs, αs)
)

# save
@save (isempty(ARGS) ? "experiment_complexity" : ARGS[1])*".dat"  str expfam μ αs T nreps algs data

