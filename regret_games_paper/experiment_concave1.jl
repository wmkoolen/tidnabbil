using Distributed;
using JLD2;


@everywhere include("runit.jl");

# setup
x = collect(range(0, 1, length=5));
μ = @. .1 + .8*4*x*(1-x);
str = Concave(x);
expfam = Gaussian();

@assert conforms(str, μ)


T = 100;
nreps = 20;


str_est = StructuredEstimator()
unc_est = UnconstrainedEstimator()
algs = ((SPk(CTracking), unc_est), (UCB(), unc_est));

# compute

# only as reproducible as the optimiser :(
@time data = pmap(
    ((i, (sr, est)),) -> runit(i, sr, str, est, expfam, μ, T),
    Iterators.product(1:nreps, algs)
)

# save
@save (isempty(ARGS) ? "experiment_concave1" : ARGS[1])*".dat"  str expfam μ T nreps algs data
