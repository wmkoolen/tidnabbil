using Distributed;
using JLD2;
using LinearAlgebra;

@everywhere include("runit.jl");

# setup

ρs = range(0, 2π, length=6)[2:end]; # drop repeated arm
ρs = vcat(ρs, ρs .+ 0.15)
x = [cos.(ρs) sin.(ρs)];
B = [argmax(x[:,2]),argmax(x[:,1])]#index of barycentric spanner
ξ = [0,2];
μ = 0.5*x*ξ; # well-specified
str = Linear(x);
expfam = Gaussian();

@assert conforms(str, μ)

T = 50000;
nreps = 200;

str_est = StructuredEstimator()
unc_est = UnconstrainedEstimator()
algs = ((SPλ(), str_est),
        (SPk(CTracking), str_est),
#        (SPk(DTracking), str_est),
        (OSSBLike(0.02/sqrt(log(T)),0), str_est),
        (UCB(), unc_est),
        (UCB(), str_est),
        (EOO(B, x, T), str_est)
        );  # UCB with str_est is LinUCB
#
# compute

# only as reproducible as the optimiser :(
@time data = pmap(
    ((i, (sr, est)),) -> runit(i, sr, str, est, expfam, μ, T),
    Iterators.product(1:nreps, algs)
)

# save
@save (isempty(ARGS) ? "experiment_linear1" : ARGS[1])*".dat"  str expfam μ T nreps algs data
