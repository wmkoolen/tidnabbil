using Distributed;
using JLD2;
using LinearAlgebra;

@everywhere include("runit.jl");

# setup

ρs = range(0, 2π, length=82)[2:end]; # drop repeated arm
x = [cos.(ρs) sin.(ρs)];
B = [argmax(x[:,2]),argmax(x[:,1])]
ξ = [1,2];
μ = x*ξ; # well-specified
str = Linear(x);
expfam = Gaussian();

@assert conforms(str, μ)


T = 1000;
nreps = 5;

str_est = StructuredEstimator()
unc_est = UnconstrainedEstimator()
algs = ((OSSBLike(0.02/sqrt(log(T)),0), str_est), (EOO(B, x, T), str_est), (SPk(CTracking), str_est), (UCB(), unc_est), (UCB(), str_est));  # UCB with str_est is LinUCB
#, SPλ(), SPk(CTracking), SPk(DTracking),OSSBLike(0.02/sqrt(log(T)), 0.)


# compute

# only as reproducible as the optimiser :(
@time data = pmap(
    ((i, (sr, est)),) -> runit(i, sr, str, est, expfam, μ, T),
    Iterators.product(1:nreps, algs)
)

# save
@save (isempty(ARGS) ? "experiment_linear2" : ARGS[1])*".dat"  str expfam μ T nreps algs data
