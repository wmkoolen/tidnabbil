using Distributed;
using JLD2;


@everywhere include("runit.jl");

# setup

μ = [.49, .17, .17, .17];
str = Simplex();
expfam = Gaussian();

@assert conforms(str, μ)


T = 100;
nreps = 20;

str_est = StructuredEstimator()
unc_est = UnconstrainedEstimator()
algs = ((SPk(CTracking), str_est), (UCB(), str_est), (SPk(CTracking), unc_est), (UCB(), unc_est));

# compute

# only as reproducible as the optimiser :(
@time data = pmap(
    ((i, (sr, est)),) -> runit(i, sr, str, est, expfam, μ, T),
    Iterators.product(1:nreps, algs)
)

# save
@save (isempty(ARGS) ? "experiment_simplex1" : ARGS[1])*".dat"  str expfam μ T nreps algs data
