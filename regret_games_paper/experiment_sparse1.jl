using Distributed;
using JLD2;


@everywhere include("runit.jl");

# setup
γ = .3;
μ = [γ, .8, γ, γ, γ, γ];
str = Sparse(1, γ);
expfam = Gaussian();

@assert conforms(str, μ)


T = 50000;
nreps = 200;

str_est = StructuredEstimator()
unc_est = UnconstrainedEstimator()
algs = (
    (SparseUCB(1, γ), str_est),
    (OSSBLike(0.02/sqrt(log(T)), 0), str_est),
    (SPλ(), str_est),
    (SPk(CTracking), str_est),
    (UCB(), unc_est),
    (UCB(), str_est),
);

# compute

# only as reproducible as the optimiser :(
@time data = pmap(
    ((i, (sr, est)),) -> runit(i, sr, str, est, expfam, μ, T),
    Iterators.product(1:nreps, algs)
)

# save
@save (isempty(ARGS) ? "experiment_sparse1" : ARGS[1])*".dat"  str expfam μ T nreps algs data
