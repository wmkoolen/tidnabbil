using Distributed;
using JLD2;


@everywhere include("runit.jl");

# setup

x = 3
γ = 0.;
µ = [γ, x, γ, x/1.5, γ, γ, γ, γ, γ, γ, γ, γ, γ, γ, γ]
str = Sparse(2, γ);
expfam = Gaussian();

@assert conforms(str, μ)


T = 50000;
nreps = 200;

str_est = StructuredEstimator()
unc_est = UnconstrainedEstimator()
algs = ((SPk(CTracking), str_est),
        (UCB(), unc_est),
        (UCB(), str_est),
        (OSSBLike(0.02/sqrt(log(T)),0), str_est),
        (SparseUCB(2, γ), str_est)
        );

# compute

# only as reproducible as the optimiser :(
@time data = pmap(
    ((i, (sr, est)),) -> runit(i, sr, str, est, expfam, μ, T),
    Iterators.product(1:nreps, algs)
)

# save
@save (isempty(ARGS) ? "experiment_sparse2" : ARGS[1])*".dat"  str expfam μ T nreps algs data
