using Distributed;
using JLD2;


@everywhere include("runit.jl");

# setup
μ = collect(range(0, 1, length=4));
str = Unconstrained();
expfam = Gaussian();

@assert conforms(str, μ)


T = 50000;
nreps = 200;

str_est = StructuredEstimator()
unc_est = UnconstrainedEstimator()
algs = ((SPk(CTracking), unc_est),
        (SPλ(), unc_est),
        (UCB(), unc_est),
        (OSSBLike(0.02/sqrt(log(T)),0), unc_est),
        );

# compute

# only as reproducible as the optimiser :(
@time data = pmap(
    ((i, (sr, est)),) -> runit(i, sr, str, est, expfam, μ, T),
    Iterators.product(1:nreps, algs)
)

# save
@save (isempty(ARGS) ? "experiment_unconstrained1" : ARGS[1])*".dat"  str expfam μ T nreps algs data
