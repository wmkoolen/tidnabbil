#################################################################
# Single lambda learner for arm j
# This is a simple meta-learner running AdaHedge on FTLs

struct λLearner
    str; # structure
    expfam;
    N;  # arm pulls
    Σθ; # sum of θs
    j;  # not the best arm
    ah::AdaHedge;
    # store outputs
    λs;
    q;

    function λLearner(str, expfam, K, j)
        l = new(str,
            expfam,
            fill(1*1e-5, K),
            fill(0.5*1e-5, K), # TODO: regularise for Bernoulli
            j,
            AdaHedge(K-1),
            [zeros(K) for k=1:K-1],
            zeros(K-1)
            );
        _update!(l);
        l
    end
end

function _update!(l::λLearner)
    l.q .= act(l.ah);
    l.λs .= [alt_min(l.str, l.expfam, l.N, l.Σθ ./ l.N, l.j, k)[2]
          for k in eachindex(l.N) if k != l.j];
end

function act(l::λLearner)
    l.q, l.λs
end

function incur!(l::λLearner, θ, kt)
    # λs and q are supposed up to date.
    ℓ = map(λ -> d(l.expfam, θ[kt], λ[kt]), l.λs);
    incur!(l.ah, ℓ);
    l.Σθ[kt] += θ[kt];
    l.N[kt]  += 1;
    _update!(l);
end
