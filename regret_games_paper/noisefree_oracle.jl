# saddle point perturbed by ϵ(t)
function sp_perturbed_k(str, expfam, μ, T, ϵ)
    K = length(μ)
    ⋆ = argmax(μ)
    Δ = maximum(μ) .- μ;

    v = zeros(T);

    ah = AdaHedge(K);

    for t in 1:T
        Δ̃ = max.(Δ, ϵ(t));

        w̃ = act(ah);
        w = w̃ ./ Δ̃ ./ sum(w̃ ./ Δ̃);

        # best response for λ-player to w (not w̃)
        v[t], λ = alt_min(str, expfam, w̃ ./ Δ̃, μ, ⋆)

        # IDEA: use linearity of alt_min in "w" here: scale w by 1/w'Δ
        # (recall that w/w'Δ̃ = w̃ ./ Δ̃) instead of scaling the
        # output. This could be (TODO: check) numerically more stable.

        # feed gain to w̃-learner
        incur!(ah, -w'Δ̃ * d.(Ref(expfam), μ, λ) ./ Δ̃);
    end

    Δ̃ = max.(Δ, ϵ(T+1));

    w̃ = act(ah);
    w = w̃ ./ Δ̃ ./ sum(w̃ ./ Δ̃);
    w̃, w, v
end

function sp_constrained_k(str::Unconstrained, expfam, µ, T)
    K = length(μ)
    J = argmax(μ)
    v = zeros(T)
    w = zeros(K)
    w̃ = zeros(K)
    for k in 1:K
        if k != J
            w[k] = 1/d(expfam, μ[k], μ[J]);
        end
    end
    Δ = [maximum(μ) - μ[k] for k in eachindex(µ)];
    v .= 1/sum(w .* Δ);
    w .= w ./ sum(w)
    w̃ .= w .* Δ ./ sum(w .* Δ)
    return w̃, w, v
end


function sp_constrained_k(str::Structure, expfam, µ, T)
    K = length(μ)
    J = argmax(μ)
    v = zeros(T);

    # Accelerate sparse case with s ==1
    if str isa Sparse
        if str.s == 1 && μ[J] != str.γ
            w = zeros(K)
            w̃ = zeros(K)
            return w̃, w, v
        end
    end

    # if str isa Unimodal
    #     w = zeros(K)
    #     w̃ = zeros(K)
    #     if J>1
    #         w[J-1] = 1/d(expfam, μ[J-1], μ[J])
    #     end
    #     if J<K
    #         w[J+1] = 1/d(expfam, μ[J+1], μ[J])
    #     end
    #     Δ = [maximum(μ) - μ[k] for k in eachindex(µ)];
    #     w .= w ./ sum(w)
    #     w̃ .= w .* Δ ./ sum(w .* Δ)
    #     return w̃, w, v
    # end

    Δ = [maximum(μ) - μ[k] for k in eachindex(µ) if k != J];
    @assert all(Δ .> 0);
    ah = AdaHedge(K-1);
    w = zeros(K);

    for t in 1:T
        w̃ = act(ah);
        w_short = w̃ ./ Δ ./ sum(w̃ ./ Δ);
        w[1:J-1] .= w_short[1:J-1]
        w[J] = 0.
        w[J+1:K] .= w_short[J:K-1]
        # best response for λ-player to w (not w̃)
        z, λ = alt_min_constr(str, expfam, w, μ, J)
        v[t] = z/w_short'Δ

        incur!(ah, -w_short'Δ * [d(expfam, μ[k], λ[k]) for k in 1:K if k != J] ./ Δ);
    end

    w̃ = act(ah);
    w_short = w̃ ./ Δ ./ sum(w̃ ./ Δ);
    w[1:J-1] .= w_short[1:J-1];
    w[J] = 0.;
    w[J+1:K] .= w_short[J:K-1];

    w̃, w, v
end


# saddle point perturbed by ϵ(t)
function sp_perturbed_λ(str, expfam, μ, T, ϵ)
    K = length(μ)
    ⋆ = argmax(μ);
    l = λLearner(str, expfam, K, ⋆);
    Δ = maximum(μ) .- μ;
    v = zeros(T);

    for t = 1:T
        q, λs = act(l);
        Δ̃ = max.(Δ, ϵ(t));

        # best response for k-player
        v[t], kt = findmax(d.(Ref(expfam), μ, hcat(λs...))*q ./ Δ̃)

        incur!(l, μ, kt);
    end

    q, λs = act(l);
    q, λs, v
end
