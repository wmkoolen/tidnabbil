
include("../expfam.jl");
include("structures.jl");

#Sparse---------------------------------------
x = 1.
#μ = [0., x, 0., x / 3, 0., 0.];
#µ=[0, x, 0, x/2, 0, 0, 0, 0, 0, 0]
µ=[0, x, 0, x/2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
str = Sparse(2, 0.);
#str = Sparse(1, 0.);
#Linear---------------------------------------
#x = 3
#θ = 2π*(2 /24)
#K = 12
#ρs = range(0, 2π, length=K+1)[1:K];
#arms = [cos.(ρs) sin.(ρs)];
#ξ = x .* [cos(θ), sin(θ)];
#μ = arms*ξ;
#str = Linear(arms);
#Simplex--------------------------------------
#x = 0.09
#μ = [0.25+3*x, 0.25-x, 0.25-x, 0.25-x]
#str = Simplex();
#Unimodal-------------------------------------
#x = 0.5
#μ=[1-4x, 1-x, 1, 1-x, 1-4x]
#μ=[1-x, 1-x, 1-x, 1, 1-x, 1-x, 1-x]
#str = Unimodal();


expfam = Gaussian();

@assert conforms(str, μ)

# rate as if we were unconstrained (the hardest case)
Δ = maximum(μ) .- μ
ds = d.(Ref(expfam), μ, maximum(μ));
rate = sum(Δ[k]/ds[k] for k in eachindex(μ) if Δ[k]>0)

# approximate real rate computation
print("apx rate computation ");
@time _,_,v = sp_perturbed_k(str, expfam, μ, 1000, t->1/t);
rate2 = 1/v[end];

println("µ=$µ")
println("unconstrained rate ", rate, "  appx rate ", rate2);

# In general, for Gaussian problems, multiply µ by x to multiply all rates by 1/x.
#
# Unconstrained1
# R = AR
#
# Sparse1
# µ=[0, x, 0, 0, 0, 0] : R = 2(K-1)/x, AR = 0
#
# Sparse2
# Two levers: level of the second arm and number of arms.
# µ=[0, x, 0, x/3, 0, 0] : R = 11/x, AR ≈ 9/x, R/AR ≈ 11/9
# µ=[0, x, 0, x/2, 0, 0] : R = 12/x, AR ≈ 4/x, R/AR ≈ 3
# µ=[0, x, 0, x/1.5, 0, 0] : R = 14/x, AR ≈ 3/x, R/AR ≈ 14/3
# µ=[0, x, 0, x/1.33, 0, 0] : R = 16/x, AR ≈ 8/x, R/AR ≈ 2
# µ=[0, x, 0, x/2, 0, 0, 0, 0, 0, 0] : R = 20/x, AR ≈ 4/x, R/AR ≈ 5
# µ=[0, x, 0, x/2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0] : R = 30/x, AR ≈ 4/x, R/AR ≈ 7.5
# 
# Simplex
# x=0.05, μ=[0.25+3*x, 0.25-x, 0.25-x, 0.25-x] : R = 30, AR = 23
# x=0.07, μ=[0.25+3*x, 0.25-x, 0.25-x, 0.25-x] : R = 21, AR = 15
# x=0.08, μ=[0.25+3*x, 0.25-x, 0.25-x, 0.25-x] : R = 18.8, AR = 13.5
# x=0.09, μ=[0.25+3*x, 0.25-x, 0.25-x, 0.25-x] : R = 16.6, AR = 6 (true structured rate: 0)
# x=0.1 , μ=[0.25+3*x, 0.25-x, 0.25-x, 0.25-x] : R = 15, AR = 0
#
# Linear
# K=6; ρs = range(0, 2π, length=K+1)[1:K]; arms = [cos.(ρs) sin.(ρs)]; ξ = [cos(θ), sin(θ)]; μ = arms*ξ;
# θ=2π/12 would make two µ equal, θ=2π/6 is aligned with one arm
# θ=2π*(1.2/12) : R = 28, AR = 26
# θ=2π*(1.5/12) : R = 14.7, AR = 8.8
# θ=2π/6 : R = 11.4, AR = 4.4
# same prblem but K=12:
# θ=2π*(1.5/24) : R = 58, AR = 41
# θ=2π/12 : R = 47.7, 15.2
#
# Unimodal
# Two levers: number of arms and shape of the decrease in mean when going away from the best arm.
# x = 0.5; μ=[1-4x, 1-x, 1, 1-x, 1-4x] : R = 10, AR = 8.5
# x = 0.5; μ=[1-2x, 1-x, 1, 1-x, 1-2x] : R = 12, AR = 8.6
# x = 0.5; μ=[1-x, 1-x, 1, 1-x, 1-x] : R = 16, AR = 8.6
# x = 0.5; μ=[1-x, 1-x, 1-x, 1, 1-x, 1-x, 1-x] : R = 24, AR = 8.6
#
#