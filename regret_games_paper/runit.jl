using Random;
using CPUTime;

include("../expfam.jl");
include("structures.jl");
include("estimators.jl");
include("samplingrules.jl");


#################################################################
# experiment running helper

function runit(seed, sr, str, est, expfam, μ, T)
    rng = MersenneTwister(seed);

    K = length(μ);
    S = zeros(K);
    N = zeros(Int64, K);

    R = zeros(T);
    Δ = maximum(μ) .- μ;

    srs = init(sr, str, est, expfam, K);

    baseline = CPUtime_us();

    for t in 1:T
        θ̂ = S ./ N;

        I = nextsample(srs, N, θ̂);

        S[I] += sample(rng, expfam, μ[I]);
        N[I] += 1;
        R[t] = N'*Δ;
    end

    R, N, S, srs, CPUtime_us()-baseline
end
