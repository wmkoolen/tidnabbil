include("../regret.jl");
include("lambda_learner.jl");
include("../tracking.jl");
include("noisefree_oracle.jl")


#################################################################
# Sampling rules

function explore_or_exploit(str, expfam, N, θ, thr)
    i = argmax(θ);
    v, closest_θ = projection(str, expfam, N, θ);
    if v < thr
        i = argmax(closest_θ);
        v2, _ = alt_min(str, expfam, N, θ, i);
        v = max(v, v2)
    end
    return i, v
end


# concentration thresholds
f(N) =  log(1+sum(N)) + .5*sum(log.(1 .+ log.(1 .+ N)))
        #1.02*log(1+sum(N));
ln̄(n, N) = log(1+n) #+ log(1+log(1+N));


# saddle point via λ-Learner
struct SPλ
end

long(sr::SPλ) = "\\lambda";

function init(l::SPλ, str::Structure, est::Estimator, expfam, K)
    SPλState(str, est, expfam, K);
end

struct SPλState
    str;
    est;
    expfam;
    λLs;
    n;
    function SPλState(str, est, expfam, K)
        new(str,
            est,
            expfam,
            λLearner.(Ref(str), Ref(expfam), K, 1:K),
            zeros(Int64, K));
    end
end

function nextsample(l::SPλState, N, θ̂)
    K = length(N)
    if sum(N) <= K-1
        return sum(N)+1
    end

    i, v = explore_or_exploit(l.str, l.expfam, N, θ̂, f(N))
    #println("t $t, v $v, log(t), $(log(t))");

    if v > f(N)
        I = i;
    else
        #θ⁺, J = findmax(θ̂ .+ sqrt.(ln̄.(l.n, N)./N)); # TODO: should use dup() in general?
        J = estimate_best_arm(l.str, l.est, l.expfam, N, θ̂)

        if N[J] ≤ l.n[J]/2 # variant to Alg. 1 line 9
            I = J;
        else
            means, confidence_inters = confidence_interval(l.str, l.est, l.expfam, N, θ̂, ln̄.(l.n[J], l.n[J]))
            #θ⁺= confidence_inters[J][2]  # can cause problems with structure
            θ⁺= dup(l.expfam, means[J], ln̄.(l.n[J], l.n[J]) / N[J])  # upper bound without structural information
            # lines 11–13
            q, λs = act(l.λLs[J]);

            ϵmax = minimum([θ⁺ - confidence_inters[k][1] for k in eachindex(N) if k != J])
            ϵ = ϵmax/(1+l.n[J]); # TODO: review
            #widths = sqrt.(ln̄.(l.n[J], N)./N);
            # println("t $t, ϵ $ϵ, widths $widths");

            UCBs = [
                maximum(ξ ->
                        q'd.(Ref(l.expfam), ξ, getindex.(λs, k))
                        /
                        max(ϵ, k!=J ? (θ⁺-ξ) : 0.),
                        [
                            confidence_inters[k][1], # conf. int. lbd
                            confidence_inters[k][2], # conf. int. ubd
                            # kink point; only if in conf. int.
                            clamp(θ⁺-ϵ, confidence_inters[k][1], confidence_inters[k][2])
                        ]
                        )
                for k in eachindex(N)];
            # println("UCBs $UCBs");
            # println("θ̂ = $θ̂\nwidths = $widths\nN=$N, n=$(l.n)");
            I = argmax(UCBs);
            incur!(l.λLs[J], θ̂, I);
        end
        l.n[J] += 1;
    end
    I;
end

# saddle point via k-Learner and tracking
struct SPk
    TrackingRule;
end

long(sr::SPk) = "k-" * abbrev(sr.TrackingRule);

function init(l::SPk, str::Structure, est::Estimator, expfam, K)
    SPkState(l.TrackingRule, str, est, expfam, K);
end


struct SPkState
    tracking;
    str;
    est;
    expfam;
    ahs;
    n;
    function SPkState(TrackingRule, str, est, expfam, K)
        new(TrackingRule(zeros(K)),
            str,
            est,
            expfam,
            [AdaHedge(K) for k in 1:K],
            zeros(Int64, K));
    end
end

function nextsample(l::SPkState, N, θ̂)
    # concentration thresholds
    K = length(N)
    if sum(N) < K
        w = zeros(K);
        w[sum(N)+1] = 1;
        return track(l.tracking, N, w);
    end

    i, v = explore_or_exploit(l.str, l.expfam, N, θ̂, f(N))
    #println("t $t, v $v, log(t), $(log(t))");

    if v > f(N)  # exploit
        w = zeros(K);
        w[i] = 1;
    else
        #println("θ̂ = $θ̂")
        J = estimate_best_arm(l.str, l.est, l.expfam, N, θ̂)

        if N[J] ≤ l.n[J]/2 # variant to Alg. 1 line 9
            w = zeros(K);
            w[J] = 1;
        else
            #widths = sqrt.(2*ln̄.(l.n[J], N)./N);
            means, confidence_inters = confidence_interval(l.str, l.est, l.expfam, N, θ̂, ln̄.(l.n[J], l.n[J]))
            #θ⁺= confidence_inters[J][2]  # can cause problems with structure
            # θ⁺= dup(l.expfam, θ̂[J], ln̄.(l.n[J], l.n[J]) / N[J])  # upper bound without structural information
            θ⁺= dup(l.expfam, means[J], ln̄.(l.n[J], l.n[J]) / N[J]) #changed to the structural mean estimate
            #println("inters ", confidence_inters)
            #println("J=$J; θ+ = $θ⁺")

            ϵmax = minimum([θ⁺ - confidence_inters[k][1] for k in 1:K if k != J])
            ϵ = ϵmax/(1+l.n[J]); # TODO: review

            Δ̃ = max.(ϵ, [θ⁺ .- confidence_inters[k][2] for k in 1:K]);

            w̃ = act(l.ahs[J]);
            w = w̃ ./ Δ̃ ./ sum(w̃ ./ Δ̃);
            # println("θ̂=$θ̂\nN=$N\nΔ̃=$Δ̃,\nw̃=$w̃,\nw=$w\ntracking=$(l.tracking)");
            _, λ = alt_min(l.str, l.expfam, w, θ̂, J);
            #println("t $t, ϵ $ϵ, widths $widths");
            UCBs = [
                maximum(ξ ->
                        d(l.expfam, ξ, λ[k])
                        /
                        max(ϵ, k!=J ? (θ⁺-ξ) : 0.),
                        [
                            confidence_inters[k][1],
                            confidence_inters[k][2],
                            # kink point; only if in conf. int.
                            clamp(θ⁺-ϵ, confidence_inters[k][1], confidence_inters[k][2])
                        ]
                        )
                for k in eachindex(N)];

            # println("UCBs $UCBs");
            # println("ϵ $ϵ")
            # println("gains $(w'Δ̃ * UCBs)")
            # println("θ̂ = $θ̂\nwidths = $widths\nN=$N, n=$(l.n)");
            incur!(l.ahs[J], -w'Δ̃ * UCBs);
            # println("Losses=$(l.ahs[J].L)")
            # println()
        end
        l.n[J] += 1;
    end
    track(l.tracking, N, w);
end

# KL-UCB or LinUCB (depending on the estimator)
struct UCB
end

long(sr::UCB) = "UCB";

function init(l::UCB, str::Structure, est::Estimator, expfam, K)
    UCBState(str, est, expfam)
end

struct UCBState
    str;  # Structure, used for confidence interval computation
    est;  # Estimator
    expfam;
end

function nextsample(l::UCBState, N, θ̂)
    width = log(1+sum(N))
    _, confidence_inters = confidence_interval(l.str, l.est, l.expfam, N, θ̂, width)
    argmax([confidence_inters[k][2] for k in 1:length(N)])
end

# Explore-exploit test with uniform exploration
struct Unif
    TrackingRule;
end

long(sr::Unif) = "Unif-" * abbrev(sr.TrackingRule);

function init(l::Unif, str::Structure, est::Estimator, expfam, K)
    UnifState(l.TrackingRule, str, expfam, K);
end


struct UnifState
    tracking;
    str;
    expfam;
    n;
    function UnifState(TrackingRule, str, expfam, K)
        new(TrackingRule(zeros(K)),
            str,
            expfam,
            zeros(Int64, K));
    end
end

function nextsample(l::UnifState, N, θ̂)
    i, v = explore_or_exploit(l.str, l.expfam, N, θ̂, f(N))
    #println("t $t, v $v, log(t), $(log(t))");

    if v > f(N)
        w = zeros(length(N));
        w[i] = 1;
    else
        w = ones(length(N)) ./ length(N);
        l.n[i] += 1;  # useless. TODO remove? what breaks?
    end
    track(l.tracking, N, w);
end

# OSSB-like exploration with our explore-exploit test
struct OSSBLike
    ϵ;
    γ;
end

long(sr::OSSBLike) = "OSSB";

function init(l::OSSBLike, str::Structure, est::Estimator, expfam, K)
    OSSBLikeState(l.ϵ, l.γ, str, est, expfam, K);
end


struct OSSBLikeState
    ϵ;  # forced exploration parameter
    γ;  # explore-exploit threshold parameter
    str;
    est;
    expfam;
    n;
    function OSSBLikeState(ϵ, γ, str, est, expfam, K)
        new(ϵ,
            γ,
            str,
            est,
            expfam,
            zeros(Int64, K));
    end
end

function nextsample(l::OSSBLikeState, N, μ̂)
    K = length(N)
    if sum(N) <= K-1
        return sum(N)+1
    end

    # concentration thresholds
    f(N) = (1+l.γ) * log(1+sum(N))
    θ̂ = estimate_means(l.str, l.est, l.expfam, N, μ̂)
    #i, v = explore_or_exploit(l.str, l.expfam, N, θ̂, f(N))
    i = argmax(θ̂)
    _, w, _ = sp_constrained_k(l.str, l.expfam, θ̂, 100) # TODO: is 100 enough?
    v, _ = alt_min_constr(l.str, l.expfam, w, θ̂, argmax(θ̂))

    # v, _ = alt_min_constr(l.str, l.expfam, N, θ̂, i)
    # if v > f(N)
    if all(N .> f(N)/v .* w)
        return i
    else
        l.n[i] += 1;
        i_least_pulled = argmin(N)
        if N[i_least_pulled] < l.ϵ * sum(l.n)
            return i_least_pulled
        else
            J = argmax(θ̂) # TODO: same as i?
            Δ = [maximum(θ̂) - θ̂[k] for k in eachindex(θ̂) if k != J];
            if any(Δ .≤ 0) || any(isnan.(Δ))
                return (sum(l.n) % K) + 1
            else
                # _, w, _ = sp_constrained_k(l.str, l.expfam, θ̂, 100)
                return argmin(N ./ w) # TODO: looks like a tracking mechanism. Use Tracking struct?
            end
        end
    end
end

#SparseUCB for sparse bandits
struct SparseUCB
    s::Int;  # Number of arms with mean > γ
    γ::Float64; # level of all K-s arms
end

long(sr::SparseUCB) = "SparseUCB";
abbrev(sr::SparseUCB) = "S-UCB";

struct SparseUCBState
    s;
    γ;
    rr; #RoundRobin counter
    function SparseUCBState(s, γ, rr)
        new(s,
            γ,
            rr);
    end
end

function init(l::SparseUCB, str::Sparse, est::Estimator, expfam, K)
    return SparseUCBState(l.s, l.γ, [1])
end

function nextsample(l::SparseUCBState, N, θ̂)
    Jt = [i for i in eachindex(θ̂) if θ̂[i] >=l.γ + sqrt(2*log(1+N[i])/N[i])]
    Kt = [i for i in eachindex(θ̂) if θ̂[i] >=l.γ + sqrt(2*log(1+sum(N))/N[i])]
    if l.rr[1] != 1 || length(Jt) < l.s
        l.rr[1] = 1 + l.rr[1] % length(θ̂)
        return l.rr[1]
    elseif length(Kt) < l.s
        for i in eachindex(Jt)
            if !(Jt[i] in Kt)
                return Jt[i]
            end
        end
    else
        UCBs = [θ̂[Kt[i]]+ sqrt(2 * log(1+sum(N))/N[Kt[i]]) for i in eachindex(Kt)]
        return Kt[argmax(UCBs)]
    end
end

#OSUB for one-dim unimodal bandits
struct OSUB
end

long(sr::OSUB) = "OSUB";

struct OSUBState
    nl #number of rounds being the leader
    expfam
end

function init(l::OSUB, str::Unimodal, est::Estimator, expfam, K)
    return OSUBState(zeros(K),expfam)
end

function nextsample(l::OSUBState, N, θ̂)
    ldr = argmax(θ̂)
    l.nl[ldr] += 1
    if (l.nl[ldr]-1)%3 == 0
        return ldr
    end
    UCBs = dup.(Ref(l.expfam), θ̂, log(l.nl[ldr])./N);
    argmax(UCBs)
end

#Algorithm proposed by Lattimore and Szepesv\'ari for linear bandits
struct EOO
    B #indexes of barycentric spanner
    xs
    T #time horizon
end

long(sr::EOO) = "Lattimore et al.";

struct EOOState
    B
    T
    expfam
    n
    phase
    μ̂
    str
    est
end

function init(l::EOO, str::Linear, est::Estimator, expfam::Gaussian, K)
    return EOOState(l.B, l.T, expfam, zeros(Int64, K), zeros(Int64, 1), zeros(K), str, est)
end

function nextsample(l::EOOState, N, θ̂)
    K, d = size(l.str.xs)
    Gram = sum([N[i] .* l.str.xs[i,:]*transpose(l.str.xs[i,:]) for i in 1:K])+ 0.1*Matrix{Float64}(I, d, d)
    inv_Gram = inv(Gram)
    # vect = sum([N[k] .* θ̂[k] .* l.str.xs[k,:] for k in eachindex(N)])
    # para = inv_Gram * vect
    # θ_str = l.str.xs * para
    θ_str = estimate_means(l.str, l.est, l.expfam, N, θ̂)
    if sum(N) <= K-1
        return sum(N)+1
    end
    if sum(N) <= d*(sqrt(log(l.T))+1)
        i = sum(N)%d +1
        l.μ̂ .= θ_str
        return l.B[i]
    end
    if l.phase[1] == 0
        l.n[argmax(θ_str)] += 1
        g = sqrt(2. *(1. +1. /log(l.T))*log(log(l.T))+2*d*log(d*log(l.T))) #Here c=2. Check how to set the constant c
        ϵ = g*maximum([sqrt(transpose(l.str.xs[i,:])* inv_Gram*l.str.xs[i,:]) for i in eachindex(θ_str)]) #G should be invertable1
        _, w, _ = sp_constrained_k(l.str, l.expfam, θ_str, 100) #w is normalized, to be changed
        v, _ = alt_min_constr(l.str, l.expfam, w, θ_str, argmax(θ_str))
        if maximum(abs.(θ_str-l.μ̂)) <= 2*ϵ
            l.μ̂ .= θ_str
            if all(N .> log(l.T)/v .* w)
                return argmax(θ_str)
            end
            return argmin(N ./ w)
        end
        l.phase[1] = 1
    end
        UCBs = dup.(Ref(l.expfam), θ_str, log(sum(N))./N);
        argmax(UCBs)
end


#CATSE for catogorized bandits
struct CATSE
    δ
end

long(sr::CATSE) = "CATSE";

struct CATSEState
    δ;
    str;
    actcate; #active category
    expfam;
end

function init(l::CATSE, str::Categorized, est::Estimator, expfam, K)
    return CATSEState(l.δ, str,zeros(1),expfam)
end

function nextsample(l::CATSEState, N, θ̂)
    K = length(N)
    if sum(N) <= K-1
        return sum(N)+1
    end
    while l.actcate[1] == 0
        β1 = sqrt(2/(N[1]+1)*(l.str.nb1*log(2)+log(1/l.δ)))
        β2 = sqrt(2/(N[l.str.nb1+1]+1)*((K-l.str.nb1)*log(2)+log(1/l.δ)))
        up1 = minimum(θ̂[1:l.str.nb1] .+ β1)
        down1 = maximum(θ̂[1:l.str.nb1] .- β1)
        up2 = minimum(θ̂[l.str.nb1+1:end] .+ β2)
        down2 = maximum(θ̂[l.str.nb1+1:end] .- β2)
        # up1 = cat_eliminate(θ̂[1:l.str.nb1], β1, 10000)
        # down1 = -cat_eliminate(-θ̂[1:l.str.nb1], β1, 10000)
        # up2 = cat_eliminate(θ̂[l.str.nb1+1:end], β2, 10000)
        # down2 = -cat_eliminate(-θ̂[l.str.nb1+1:end], β2, 10000)
        if down1 > up2
            l.actcate[1] = 1
            # println(l.actcate[1])
            # println(N)
            # println(down1)
            # println(up1)
            # println(down2)
            # println(up2)
        elseif down2 > up1
            l.actcate[1] = 2
            # println(l.actcate[1])
            # println(N)
            # println(down1)
            # println(up1)
            # println(down2)
            # println(up2)
        end
        return argmin(N)
    end

    UCBs = zeros(K)
    if l.actcate[1] == 1
        UCBs[1:l.str.nb1] = dup.(Ref(l.expfam), θ̂[1:l.str.nb1], log(sum(N))./N[1:l.str.nb1])
    elseif l.actcate[1] == 2
        UCBs[l.str.nb1+1:end] = dup.(Ref(l.expfam), θ̂[l.str.nb1+1:end], log(sum(N))./N[l.str.nb1+1:end])
    end
    argmax(UCBs)
end

# function cat_eliminate(w, β ,T) #minimize <x,w> + β\lVert x \rVert over ∑x_i ≦ 1
#     dim = length(w)
#
#     if dim == 1
#         x = ones(1)
#         return x'w + β
#     end
#
#     x = ones(dim)
#     for t in 1:T
#         η = 1/sqrt(t)
#         ∇f = w .+ β .* x ./ sqrt(x'x)
#         x_last = copy(x)
#         x .= x .- η .* ∇f
#         x .= max.(x, 0)
#         x .= x ./sum(x)
#
#         if sqrt(transpose(x .- x_last)*(x .- x_last)) <= 0.001
#             break
#         end
#     end
#
#     x'w + β*sqrt(x'x)
# end
