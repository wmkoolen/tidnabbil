# compute the w̃ (and w) components of a saddle point
# using best-response-oracle calls for λ
using Plots
include("../regret.jl");
include("../expfam.jl");
include("structures.jl");
include("lambda_learner.jl");
include("noisefree_oracle.jl")




function graphit()
    Ts = round.(Int64, exp.(range(log(1), log(1e5), length=6)));
    # fixed epsilon functions
    ϵs = exp.(range(log(1e-10), log(1e-1), length=50))
    ϵf = map(ϵ ->(t->ϵ), ϵs);

    v1 = getindex.(sp_perturbed_k.(Ref(str), Ref(expfam), Ref(μ), Ts[end], ϵf), 3);
    v2 = getindex.(sp_perturbed_λ.(Ref(str), Ref(expfam), Ref(μ), Ts[end], ϵf), 3);

    plot(title="$str $μ", xlabel="epsilon")
    for t in Ts
        plot!(ϵs, [1 ./ getindex.(v1, t) 1 ./ getindex.(v2, t)], label=["k $t" "lambda $t"], xscale=:log10)
    end
    gui()
end


# print some info about saddle point solution
function print_sp(str, expfam, μ, T)

    @assert conforms(str, μ)

    println("μ $μ");

    if str isa Unconstrained
        Δ = maximum(μ) .- μ
        ds = d.(Ref(expfam), μ, maximum(μ));
        nrmzd(w) = w ./ sum(w);

        println("\nConstrained, closed-form")
        println("quality $(1/sum(Δ[Δ.>0] ./ ds[Δ.>0]))");
        println("w̃ $(nrmzd(Δ[Δ.>0] ./ ds[Δ.>0]))̃")
        println("w $(nrmzd(1 ./ ds[Δ.>0]))")
    end


    ϵ(t) = 1/t;
    println("\nPerturbed, w-learner");
    w̃, w, v = sp_perturbed_k(str, expfam, μ, T, ϵ);
    println("quality is $(v[end]), rate is $(1/v[end])");
    println("w̃ $w̃")
    println("w $w")


    println("\nPerturbed, λ-learner");
    q, λs, v2 = sp_perturbed_λ(str, expfam, μ, T, ϵ);
    println("quality is $(v2[end]), rate is $(1/v2[end])");
    println("q is $q");
    println(join(λs, "\n"));
end



T = 10000;
μ = [.5, .8, .3, .3, .3, .3]; #[.4, .2, .6, .1, .8, .2, .5]; # collect(range(0.1, .9, length=4))
#str = Lipschitz(collect(1:length(μ)), .3);
#μ = 6*[.4, .3, .2, .1];
str = Sparse(2, .3)
#str = Unconstrained(); # Simplex()
expfam = Gaussian();

print_sp(str, expfam, μ, T);
