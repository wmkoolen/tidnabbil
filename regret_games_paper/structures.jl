# This is about bandit structure. That is, a set of bandit instances.
#
# For a given J, we need to reason about ¬J, i.e. the subset of the
# domain under the constraint that J is not the best arm.
#
# For ¬J we need to support the following operations:
#
# - Best response. I.e. give the ML λ under the constraint that arm J
#   is not the best. This is so that we can implement a w-learner.
#
# - Cover ¬J with a tractable number of convex cells. We the need to
#   be able to compute the ML on each cell. This is so that we
#   implement a λ-learner based on λ-FTLs.
#
#
# Here we make the assumption that we can conver ¬J with cells indexed
# by {k ∈ 1:K | k != J}. The typical examples of this are either {λ :
# λ_k > λ_J} or {λ : k = \argmax_i λ_i}


using JuMP, OSQP, MathOptInterface, LinearAlgebra;

include("PAVA.jl")


abstract type Structure
end

# find the best element of ¬J by searching over all k != J
function alt_min(str::Structure, expfam, N, θ, J)
    minimum(alt_min(str, expfam, N, θ, J, k) for k in eachindex(θ) if k != J)
end

# find the best element λ of ¬J by searching over all k != J
# under the additional constraint that \max_i λ_i = θ_J
function alt_min_constr(str::Structure, expfam, N, θ, J)
    minimum(alt_min_constr(str, expfam, N, θ, J, k) for k in eachindex(θ) if k != J)
end

function conforms(str::Structure, µ)
    tol = 1e-6
    # this is a lazy way to check conformity: checking whether μ is
    # close to its projection. Structures are expected to provide a
    # direct, efficient implementation. Also, here we pray your
    # structure can handle Gaussians
    projection(str, Gaussian(), ones(length(µ)), µ)[1] < tol
end

#####################################################################

# No constraints between arms. Classical multi-armed bandit problem.
struct Unconstrained  <: Structure
end

function conforms(str::Unconstrained, θ)
    true
end

# projection on the structure
function projection(str::Unconstrained, expfam, N, θ)
    0, θ;
end

# projection on ¬J ∩ {λ[k] ≥ λ[J]}
function alt_min(str::Unconstrained, expfam, N, θ, J, k)
    @assert k != J;
    if θ[J] ≤ θ[k]
        0, θ
    else
        λ = copy(θ);
        q = (N[J]*θ[J]+N[k]*θ[k])/(N[J]+N[k]);
        λ[J] = λ[k] = q;
        v = N[J]*d(expfam, θ[J], q) + N[k]*d(expfam, θ[k], q);
        v, λ
    end
end

# projection on ¬J ∩ {λ[k] ≥ λ[J]} ∩ {λ[J] = θ[J]}
function alt_min_constr(str::Unconstrained, expfam, N, θ, J, k)
    @assert k != J;
    if θ[J] ≤ θ[k]
        0, θ
    else
        λ = copy(θ);
        λ[k] = θ[J]
        v = N[k]*d(expfam, θ[k], λ[k]);
        v, λ
    end
end

function rate(str::Unconstrained, expfam, θ)
    ⋆ = argmax(θ);
    λs = [begin
          λ = copy(θ);
          λ[k] = θ[⋆];
          λ
          end
          for k in 1:length(θ)
          if k != ⋆];
    Δ = θ[⋆] .- θ;
    D = d.(Ref(expfam), θ, hcat(λs...));
    v, w, q = solve(D[Δ.>0,:] ./ Δ[Δ.>0]);
    1/v, w, q, λs
    # TODO use this as template for other structures,
    # then replace it here by sum(Δ[Δ.>0] ./ d.(...)[Δ.>0])
end



#####################################################################
# All suboptimal arms are at the fixed, known level γ
#
# BIG FAT WARNING: the set ¬J ∩ {λ[k] is max} is only convex for s=1
# so this cannot be used in a λ-learner for s>1.
# TODO: decompose the domain into \binom{k}{s} convex sets.
#
struct Sparse <: Structure
    s::Int;  # Number of arms with mean > γ
    γ::Float64; # level of all K-s arms
end

function conforms(str::Sparse, θ)
    tol = 1e-4;
    all(θ .≥ str.γ - tol) && (sum(θ .> str.γ + tol) <= str.s)
end

function projection(str::Sparse, expfam, N, θ)
    # enforce ≥ γ constraint
    λ = max.(θ, str.γ);
    # vector of cost of moving everyone to γ
    Nd = N .* d.(Ref(expfam), λ, str.γ);
    p = sortperm(Nd);
    for i=1:(length(N)-str.s)
        λ[p[i]] = str.γ;
    end
    N'd.(Ref(expfam), θ, λ), λ
end

# projection on ¬J ∩ {λ[k] is max}
function alt_min(str::Sparse, expfam, N, θ, J, k)
    @assert k != J;
    if str.s == 1
        λ = fill(str.γ, length(θ));
        λ[k] = max(str.γ, θ[k]);
        return N'd.(Ref(expfam), θ, λ), λ
    end
    K = length(N)
    λ = max.(θ, str.γ)
    p = sortperm(N .* d.(Ref(expfam), λ, str.γ))
    for i=1:(K-str.s)
        λ[p[i]] = str.γ;
    end

    if λ[k] < λ[J] # project to enforce constraint
        # Possible cases:
        # - k sent to γ and j>γ -> get k back to its value, send next one to γ. Then
        #   - either j=γ and nothing to do
        #   - or j>γ. If j>k then equalize.
        # - k and j not sent to γ. equalize.
        if λ[k] == str.γ
            λ[k] = θ[k]
            λ[p[K-str.s+1]] = str.γ
        end
        if λ[k] < λ[J]
            λ[k] = λ[J] = (N[k]*θ[k] + N[J]*θ[J]) / (N[k] + N[J]);
        end
    end
    N'd.(Ref(expfam), θ, λ), λ
end

# projection on ¬J ∩ {λ[k] ≥ λ[J]} ∩ {λ[J] = θ[J]}
function alt_min_constr(str::Sparse, expfam, N, θ, J, k)
    @assert k != J;
    if θ[J] < str.γ  # the constrained alternative set is empty
        return Inf, θ
    end
    if str.s == 1
        λ = fill(str.γ, length(θ));
        λ[J] = θ[J];
        if λ[J] == str.γ
            λ[k] = max(str.γ, θ[k]);
            return N'd.(Ref(expfam), θ, λ), λ
        else
            return Inf, θ  # we cannot have 1-sparse and λ[k] ≥ λ[J]
        end
    end
    # Now we know that θ[J] ≥ str.γ
    K = length(N)
    λ = max.(θ, str.γ)
    if λ[J] > str.γ
        # The K-1 other coordinates of λ are str.s-1 sparse.
        nb_non_γ = str.s-1
    else
        # The K-1 other coordinates of λ are str.s sparse.
        nb_non_γ = str.s
    end
    p = sortperm([N[k] * d(expfam, λ[k], str.γ) for k in 1:K if k != J])
    for i=1:(K-1-nb_non_γ)
        pos_i = p[i]
        λ[pos_i + sum(pos_i ≥ J)] = str.γ;
    end
    if λ[k] < λ[J] # project to enforce constraint
        # Possible cases:
        # - λ[k] sent to γ -> get λ[k] to λ[J], send next one to γ.
        # - λ[k] not sent to γ -> get λ[k] to λ[J].
        if λ[k] == str.γ
            pos = p[K-nb_non_γ]
            λ[pos + sum(pos ≥ J)] = str.γ
        end
        if λ[k] < λ[J]
            λ[k] = λ[J]
        end
    end
    N'd.(Ref(expfam), θ, λ), λ
end


#####################################################################
# Categorized Bandits
# For now only two categories

struct Categorized <: Structure
    nb1::Int;  # Number of arms in category 1
end

function conforms(str::Categorized, θ)
    tol = 1e-6;
    (minimum(θ[1:str.nb1]) - maximum(θ[str.nb1+1:end]) > -tol) || (minimum(θ[str.nb1+1:end]) - maximum(θ[1:str.nb1]) > -tol)
end


# takes a convex function and its derivative
# returns the minimum and minimiser
function bs_min(ξ, f, df, lo, hi)
    λ = ξ(binary_search(γ -> df(ξ(γ)), lo, hi));
    f(λ), λ;
end


function projection(str::Categorized, expfam, N, θ)
    # consider making either the left or the right category the best
    λl(γ) = [max.(γ, θ[1:str.nb1]) ..., min.(γ, θ[str.nb1+1:end])...];
    λr(γ) = [min.(γ, θ[1:str.nb1]) ..., max.(γ, θ[str.nb1+1:end])...];

    minimum(bs_min(λ,
                   λ -> N'd.(Ref(expfam), θ, λ),
                   λ -> N'dλ_d.(Ref(expfam), θ, λ),
                   extrema(θ)...)
            for λ in (λl, λr));
end

# projection on ¬J ∩ {λ[k] ≥ λ[J]}
function alt_min(str::Categorized, expfam, N, θ, J, k)
    @assert k != J;

    # we need to make k better than J and project on the structure...
    λ(γ) = begin
        z = if k ≤ str.nb1
            [max.(γ, θ[1:str.nb1]) ..., min.(γ, θ[str.nb1+1:end])...];
        else
            [min.(γ, θ[1:str.nb1]) ..., max.(γ, θ[str.nb1+1:end])...];
        end
        if z[k] < z[J] # can only happen if k,J in same category
            z[k] = z[J] = max(γ, (N[k]*θ[k]+N[J]*θ[J])/(N[k]+N[J]));
        end
        z
    end

    bs_min(λ,
           λ -> N'd.(Ref(expfam), θ, λ),
           λ -> N'dλ_d.(Ref(expfam), θ, λ),
           extrema(θ)...);
end

function grad_alt_min_constr_fixed_best(str::Categorized, expfam, N, θ, J, k, γ)
    @assert all([J,k] .> str.nb1) || all([J,k] .<= str.nb1)
    λ = copy(θ)
    λ[k] = λ[J]
    if k <= str.nb1
        λ[1:str.nb1] .= max.(γ, λ[1:str.nb1])
        λ[str.nb1+1:end] .= min.(γ, λ[str.nb1+1:end])
    else
        λ[1:str.nb1] .= min.(γ, λ[1:str.nb1])
        λ[str.nb1+1:end] .= max.(γ, λ[str.nb1+1:end])
    end
    # NOTE: λ[k] is not a function of γ (neither is λ[J], but derivative is zero already)
    return sum(N[i]*dλ_d(expfam, θ[i], λ[i]) for i in eachindex(N) if i != k);
end


# projection on ¬J ∩ {λ[k] ≥ λ[J]} ∩ {λ[J] = θ[J]}
function alt_min_constr(str::Categorized, expfam, N, θ, J, k)
    @assert k != J
    @assert all(θ[J] .≥ θ)
    λ = copy(θ)
    if J <= str.nb1 && k > str.nb1
        λ[str.nb1+1:end] .= λ[J]
    elseif J > str.nb1 && k <= str.nb1
        λ[1:str.nb1] .= λ[J]
    else
        γ = binary_search(x -> grad_alt_min_constr_fixed_best(str, expfam, N, θ, J, k, x), minimum(θ), maximum(θ))
        λ[k] = λ[J]
        if k <= str.nb1
            λ[1:str.nb1] .= max.(γ, λ[1:str.nb1])
            λ[str.nb1+1:end] .= min.(γ, λ[str.nb1+1:end])
        else
            λ[1:str.nb1] .= min.(γ, λ[1:str.nb1])
            λ[str.nb1+1:end] .= max.(γ, λ[str.nb1+1:end])
        end
    end
    N'd.(Ref(expfam), θ, λ), λ
end

#####################################################################
# currently interfaced for Lipschitz in 1D, but the appraoch is generic
struct Lipschitz <: Structure
    D; # metric

    function Lipschitz(x, L)
        @assert issorted(x);
        new(L.*abs.(x[2:end] .- x[1:end-1]));
    end
end

function conforms(str::Lipschitz, θ)
    tol = 1e-3;
    all(abs.(diff(θ)) .≤ str.D .+ tol)
end


# TODO: projection and alt_min differ by one line (the constraint that k
# is best). Merge them.
function projection(str::Lipschitz, expfam::Gaussian, N, θ)
    model = Model(with_optimizer(OSQP.Optimizer, verbose=false, polish=true));
    K = length(θ);

    @variable(model, λ[1:K]);

    # Lipschitz
    @constraint(model, -str.D .≤ (λ[2:end] .- λ[1:end-1]) .≤ str.D);

    # weighted squared loss
    @objective(model, Min, N'*((λ .- θ).^2)/(2*expfam.σ2));

    optimize!(model);
    @assert termination_status(model) == MathOptInterface.OPTIMAL;

    objective_value(model), value.(λ);
end


function alt_min(str::Lipschitz, expfam::Gaussian, N, θ, J, k)
    @assert k != J;
    model = Model(with_optimizer(OSQP.Optimizer, verbose=false, polish=true));
    K = length(θ);

    @variable(model, λ[1:K]);

    # Lipschitz
    @constraint(model, -str.D .≤ (λ[2:end] .- λ[1:end-1]) .≤ str.D);

    # k is best
    @constraint(model, λ[k] .≥ λ);

    # weighted squared loss
    @objective(model, Min, N'*((λ .- θ).^2)/(2*expfam.σ2));

    optimize!(model);
    @assert termination_status(model) == MathOptInterface.OPTIMAL;

    objective_value(model), value.(λ);
end





function alt_min_constr(str::Lipschitz, expfam::Gaussian, N, θ, J, k)
    @assert k != J;
    model = Model(with_optimizer(OSQP.Optimizer, verbose=false, polish=true));
    K = length(θ);

    @variable(model, λ[1:K]);

    # Lipschitz
    @constraint(model, -str.D .≤ (λ[2:end] .- λ[1:end-1]) .≤ str.D);

    # k is best
    @constraint(model, λ[k] .≥ λ);

    # λ[J] = θ[J]
    @constraint(model, λ[J] == θ[J]);

    # weighted squared loss
    @objective(model, Min, N'*((λ .- θ).^2)/(2*expfam.σ2));

    optimize!(model);
    @assert termination_status(model) == MathOptInterface.OPTIMAL;

    objective_value(model), value.(λ);
end



#####################################################################
struct Unimodal <: Structure
end

function conforms(str::Unimodal, θ)
    tol = 1e-4;
    k = argmax(θ);
    all(θ[1:k-1] .≤ θ[2:k] .+ tol) &&
    all(θ[k:end-1] .≥ θ[k+1:end] .- tol)
end


function projection(str::Unimodal, expfam::Gaussian, N, θ)
    v, λ = PAVA_project(N, θ)
    v/expfam.σ2, λ
end

function alt_min(str::Unimodal, expfam::Gaussian, N, θ, J)
    v, λ = PAVA_alt_min(N, θ, J)
    v/expfam.σ2, λ
end

function alt_min(str::Unimodal, expfam::Gaussian, N, θ, J, k)
    v, λ = PAVA_alt_min(N, θ, J, k)
    v/expfam.σ2, λ
end

function alt_min_constr(str::Unimodal, expfam, N, θ, J)
    v, λ = PAVA_alt_min_constr(N, θ, J)
    v/expfam.σ2, λ
end




#####################################################################
# Arm vector from the probability simplex
#
# Something is weird with this structure: Suppose we have a model with
# a best arm μ^* > 0.5. Then to find a model in Alt(*), we need to
# lower the * entry. The oracle strategy is hence to only sample arm
# *. This blows up the constraint at zero regret cost. So there is no
# asymptotic log(T) component to the regret at all.

struct Simplex <: Structure
end

function conforms(str::Simplex, θ)
    tol = 5e-2;
    all(θ .≥ -tol) && abs(1-sum(θ)) < tol
end


# TODO: projection and alt_min differ by one line (the constraint that k
# is best). Merge them.
function projection(str::Simplex, expfam::Gaussian, N, θ)
    model = Model(with_optimizer(OSQP.Optimizer, verbose=false, polish=true));
    K = length(θ);

    @variable(model, λ[1:K]);

    # simplex
    @constraint(model, λ .≥ 0);
    @constraint(model, sum(λ) == 1);

    # weighted squared loss
    @objective(model, Min, N'*((λ .- θ).^2)/(2*expfam.σ2));

    optimize!(model);
    @assert termination_status(model) == MathOptInterface.OPTIMAL;

    objective_value(model), value.(λ);
end

function alt_min(str::Simplex, expfam::Gaussian, N, θ, J, k)
    @assert k != J;
    model = Model(with_optimizer(OSQP.Optimizer, verbose=false, polish=true));
    K = length(θ);

    @variable(model, λ[1:K]);

    # simplex
    @constraint(model, λ .≥ 0);
    @constraint(model, sum(λ) == 1);

    # k is best
    @constraint(model, λ[k] .≥ λ);

    # weighted squared loss
    @objective(model, Min, N'*((λ .- θ).^2)/(2*expfam.σ2));

    optimize!(model);
    @assert termination_status(model) == MathOptInterface.OPTIMAL;

    objective_value(model), value.(λ);
end



function alt_min_constr(str::Simplex, expfam::Gaussian, N, θ, J, k)
    @assert k != J;
    model = Model(with_optimizer(OSQP.Optimizer, verbose=false, polish=true));
    K = length(θ);

    @variable(model, λ[1:K]);

    # simplex
    @constraint(model, λ .≥ 0);
    @constraint(model, sum(λ) == 1);

    # k is best
    @constraint(model, λ[k] .≥ λ);

    # λ[J] = θ[J]
    @constraint(model, λ[J] == θ[J]);

    # weighted squared loss
    @objective(model, Min, N'*((λ .- θ).^2)/(2*expfam.σ2));

    optimize!(model);

    if termination_status(model) == MathOptInterface.INFEASIBLE
        return Inf, θ
    end

    @assert termination_status(model) == MathOptInterface.OPTIMAL;

    objective_value(model), value.(λ);
end




#####################################################################
# the model is that θ_i = a x_i + b for some parameters a and b
# Don't use it. Use linear with feature vectors [x_i, 1]
struct Affine <: Structure
    xs;
end


# note: we use the default conforms (which calls projection)


function projection(str::Affine, expfam::Gaussian, N, θ)
    model = Model(with_optimizer(OSQP.Optimizer, verbose=false, polish=true));

    @variable(model, a);
    @variable(model, b);

    # weighted squared loss
    @objective(model, Min, N'*((a .* str.xs .+ b .- θ).^2)/(2*expfam.σ2));

    optimize!(model);
    @assert termination_status(model) == MathOptInterface.OPTIMAL;

    objective_value(model), value(a) .* str.xs .+ value(b);
end


function alt_min(str::Affine, expfam::Gaussian, N, θ, J, k)
    @assert k != J;
    model = Model(with_optimizer(OSQP.Optimizer, verbose=false, polish=true));

    @variable(model, a);
    @variable(model, b);

    # k is best arm
    @constraint(model, a .* (str.xs[k] .- str.xs) .≥ 0)

    # weighted squared loss
    @objective(model, Min, N'*((a .* str.xs .+ b .- θ).^2)/(2*expfam.σ2));

    optimize!(model);
    @assert termination_status(model) == MathOptInterface.OPTIMAL;

    objective_value(model), value(a) .* str.xs .+ value(b);
end


function alt_min_constr(str::Affine, expfam::Gaussian, N, θ, J, k)
    @assert k != J;
    model = Model(with_optimizer(OSQP.Optimizer, verbose=false, polish=true));

    @variable(model, a);
    @variable(model, b);

    # k is best arm
    @constraint(model, a .* (str.xs[k] .- str.xs) .≥ 0)

    # λ[J] = θ[J]
    @constraint(model, a*str.xs[J]+b == θ[J])

    # weighted squared loss
    @objective(model, Min, N'*((a .* str.xs .+ b .- θ).^2)/(2*expfam.σ2));

    optimize!(model);
    @assert termination_status(model) == MathOptInterface.OPTIMAL;

    objective_value(model), value(a) .* str.xs .+ value(b);
end



#####################################################################
# the model is that θ_i = ⟨x_i, ξ⟩ for some parameter vector ξ in Rᵈ
struct Linear <: Structure
    xs; #K × d array
end

function conforms(str::Structure, µ)
    tol = 1e-4
    # solve linear system to get parameters
    ξ = str.xs \ μ;
    # and check squared reconstruction error
    sum((str.xs*ξ .- μ).^2) < tol
end


function projection(str::Linear, expfam::Gaussian, N, µ)

    Gram = str.xs'*Diagonal(N)*str.xs+0.1*I; # TODO: why is regularisation correct/useful?
    vect = str.xs'*(μ.*N);

    ξ = Gram \ vect
    λ = str.xs * ξ

    return N'd.(Ref(expfam), µ, λ), λ
end

# projection on ¬J ∩ {λ[k] ≥ λ[J]}
function alt_min(str::Linear, expfam::Gaussian, N, µ, J, k)
    @assert k != J;

    # first compute regular projection
    Gram = str.xs'*Diagonal(N)*str.xs+0.1*I; # TODO: why is regularisation correct/useful?
    vect = str.xs'*(μ.*N);
    ξ = Gram \ vect;
    λ = str.xs * ξ;

    if λ[k] < λ[J] # still need to make k better than J
        direction = str.xs[k,:] - str.xs[J,:]
        Vinv_dir = Gram \ direction
        η = (ξ'direction) / (direction'Vinv_dir)
        ξ = ξ .- η .* Vinv_dir
        λ = str.xs * ξ
        @assert isapprox(λ[k], λ[J], atol=1e-4)
    end
    N'd.(Ref(expfam), µ, λ), λ
end


# projection on ¬J ∩ {λ[k] ≥ λ[J]} ∩ {λ[J] = θ[J]}
function alt_min_constr(str::Linear, expfam::Gaussian, N, μ, J, k)
    @assert k != J;

    Gram = str.xs'*Diagonal(N)*str.xs+0.1*I; # TODO: why is regularisation correct/useful?
    inv_Gram = inv(Gram);
    vect = str.xs'*(μ.*N);

    # compute projection on the structure s.t. λ[J] = θ[J]
    JAv = str.xs[J,:]'inv_Gram*vect;
    JAJ = str.xs[J,:]'inv_Gram*str.xs[J,:];
    q = (JAv - μ[J])/JAJ;
    ξ = inv_Gram * (vect .- q*str.xs[J,:]);

    @assert str.xs[J,:]'ξ ≈ μ[J];

    if str.xs[k,:]'ξ < μ[J] # ok, we need to also enforce λ[k] = θ[J]
        kAv = str.xs[k,:]'inv_Gram*vect;
        kAk = str.xs[k,:]'inv_Gram*str.xs[k,:];
        kAJ = str.xs[k,:]'inv_Gram*str.xs[J,:];
        det = JAJ*kAk-kAJ^2;

        p = ( kAJ*(JAv-μ[J]) - JAJ*(kAv-μ[J]))/det;
        q = (-kAk*(JAv-μ[J]) + kAJ*(kAv-μ[J]))/det

        ξ = inv_Gram *(vect .+ p*str.xs[k,:] .+ q*str.xs[J,:]);

        @assert str.xs[J,:]'ξ ≈ μ[J];
        @assert str.xs[k,:]'ξ ≈ μ[J];
    end

    λ = str.xs * ξ

    @assert λ[J] ≈ μ[J] "$(λ[J]) vs $(μ[J])"
    @assert λ[J] ≤ λ[k] + 1e-5

    return N'd.(Ref(expfam), μ, λ), λ
end


#####################################################################
# the means are a concave function of x
struct Concave <: Structure
    dx;

    function Concave(x)
        @assert issorted(x);
        new(diff(x));
    end
end

function conforms(str::Concave, θ)
    tol = 1e-3;
    all(diff(diff(θ) ./ str.dx) .<= tol)
end


# TODO: projection and alt_min differ by one line (the constraint that k
# is best). Merge them.
function projection(str::Concave, expfam::Gaussian, N, θ)
    model = Model(with_optimizer(OSQP.Optimizer, verbose=false, polish=true));
    K = length(θ);

    @variable(model, λ[1:K]);

    # Concave
    @constraint(model,
                (λ[3:end-0] .- λ[2:end-1]) ./ str.dx[2:end-0] .≤
                (λ[2:end-1] .- λ[1:end-2]) ./ str.dx[1:end-1]);

    # weighted squared loss
    @objective(model, Min, N'*((λ .- θ).^2)/(2*expfam.σ2));

    optimize!(model);
    @assert termination_status(model) == MathOptInterface.OPTIMAL;

    objective_value(model), value.(λ);
end


function alt_min(str::Concave, expfam::Gaussian, N, θ, J, k)
    @assert k != J;
    model = Model(with_optimizer(OSQP.Optimizer, verbose=false, polish=true));
    K = length(θ);

    @variable(model, λ[1:K]);

    # Concave
    @constraint(model,
                (λ[3:end-0] .- λ[2:end-1]) ./ str.dx[2:end-0] .≤
                (λ[2:end-1] .- λ[1:end-2]) ./ str.dx[1:end-1]);

    # k is best
    @constraint(model, λ[k] .≥ λ);

    # weighted squared loss
    @objective(model, Min, N'*((λ .- θ).^2)/(2*expfam.σ2));

    optimize!(model);
    @assert termination_status(model) == MathOptInterface.OPTIMAL;

    objective_value(model), value.(λ);
end



function alt_min_constr(str::Concave, expfam::Gaussian, N, θ, J, k)
    @assert k != J;
    model = Model(with_optimizer(OSQP.Optimizer, verbose=false, polish=true));
    K = length(θ);

    @variable(model, λ[1:K]);

    # Concave
    @constraint(model,
                (λ[3:end-0] .- λ[2:end-1]) ./ str.dx[2:end-0] .≤
                (λ[2:end-1] .- λ[1:end-2]) ./ str.dx[1:end-1]);

    # k is best
    @constraint(model, λ[k] .≥ λ);

    # λ[J] = θ[J]
    @constraint(model, λ[J] == θ[J]);

    # weighted squared loss
    @objective(model, Min, N'*((λ .- θ).^2)/(2*expfam.σ2));

    optimize!(model);
    @assert termination_status(model) == MathOptInterface.OPTIMAL;

    objective_value(model), value.(λ);
end
