include("../../expfam.jl");
include("../../peps.jl");
include("../structures.jl");
include("../estimators.jl");
include("../samplingrules.jl");
include("../../regret.jl");
include("../lambda_learner.jl");
include("../noisefree_oracle.jl");


using Test
@testset "main" begin
    include("samplingrules.jl");
    include("structures.jl");
    include("test_PAVA.jl");
    include("regret.jl");
    include("rate.jl");
end
