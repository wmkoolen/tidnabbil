@testset "rate" begin
    #μ = collect(range(0, 1, length=4));
    μ = randn(4);
    str = Unconstrained();
    expfam = Gaussian();

    # known-good answer for Unconstrained
    Δ = maximum(μ) .- μ
    ds = d.(Ref(expfam), μ, maximum(μ));
    rt = sum(Δ[k]/ds[k] for k in eachindex(μ) if Δ[k]>0)

    # approximate saddle point of perturbed problem
    ϵ(t) = 1/t;
    T = 50000;
    @time w̃, w,  vk = sp_perturbed_k(str, expfam, μ, T, ϵ);
    @time q, λs, vλ = sp_perturbed_λ(str, expfam, μ, T, ϵ);
    @time w̃c,wc, vc = sp_constrained_k(str, expfam, μ, T);

    println("$w̃c, $wc, $(vc[end])")

    # alt_min makes sense
    @test all(vk .≤ vλ)

    # perturbations make it worse
    @test all(vk .≤ 1/rt)

    # constraints make it worse
    @test all(vc .≤ 1/rt)

    # perturbations have larger gaps than constraints
    @test all(vk .≤ vc)

    # vλ is an upper bound up to given precision:
    @test all(1/rt .≤ vλ.+sqrt.(2 .* ϵ.(1:T) .* vλ));

#    plot([vk vλ vλ.+sqrt.(2 .* ϵ.(1:T) .* vλ) vc 0vλ.+1/rt],
#         label=["k"  "\\lambda"  "\\lambda-bd"  "c" "real"],
#         ylim=(0.05,0.15))
end
