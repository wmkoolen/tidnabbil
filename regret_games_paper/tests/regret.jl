

function spin(μ, T)
    K = length(μ)
    a = AdaHedge(K);
    for t in 1:T
        incur!(a, randn(K) .+ μ);
    end

    @test isfinite(a.Δ)
end



T = 1000;

@testset "AH spins" begin
    for K in 1:5
        for it in 1:100
            spin(zeros(K), T); # sort-of worst case
            spin(randn(K), T); # one clear winner
        end
    end
end

