@testset "structures" begin
    expfam = Gaussian();

    @testset "unconstrained" begin
        str = Unconstrained();
        θ = [.3, 0., .8, .5, .4];
        N = [1, 1, 1, 1, 1];

        @test conforms(str, θ);
        cost_proj, proj = projection(str, expfam, N, θ);
        @test  cost_proj == 0;
        @test all(proj .== θ);

        @test_throws AssertionError alt_min(str, expfam, N, θ, 1, 1)
        cost, λ = alt_min(str, expfam, N, θ, 1, 2)
        @test cost == N' * d.(Ref(expfam), θ, λ);
        @test λ[1] == λ[2];
        @test all(λ[3:5] .== θ[3:5]);
    end

    @testset "sparse" begin
        θ = [.3, 0., .8, .5, .4];
        N = [1, 1, 2, 2, 10];
        # Sparse s=1
        str = Sparse(1, .3);
        @test !conforms(str, θ);
        @test conforms(str, [.3, .5, .3, .3]);

        cost_proj, proj = projection(str, expfam, N, θ);
        @test all(proj .== [.3, .3, .8, .3, .3]);

        @test all(alt_min(str, expfam, N, θ, 1, 3)[2] .== proj);

        # Sparse s>1
        str = Sparse(2, .3);
        @test !conforms(str, θ);
        @test conforms(str, [.3, .3, .5, .6]);

        cost_proj, proj = projection(str, expfam, N, θ);

        @test all(proj .== [.3, .3, .8, .3, .4]);

        @test_throws AssertionError alt_min(str, expfam, N, θ, 1, 1);
        @test all(alt_min(str, expfam, N, θ, 1, 3)[2] .== proj);
        cost, λ = alt_min(str, expfam, N, θ, 3, 5);
        @test cost == N'd.(Ref(expfam), θ, λ);
        @test sum(λ .> .3) == 2;
        @test λ[3] == λ[5];
    end


end







#####################################################################
# Excercise bandit structure.

# wrapper to get nicer output from julia test framework
function test_structure(str, expfam, N, θ)
    @testset "$(typeof(str))" begin
        _test_structure(str, expfam, N, θ)
    end
end

function _test_structure(str, expfam, N, θ)
    K = length(θ);

    tol = 2e-3;

    # compute projection
    cv, cλ = projection(str, expfam, N, θ);
    @test conforms(str, cλ)
    @test abs(cv - N'd.(Ref(expfam), θ, cλ)) < tol; # check distance
    cJ = argmax(cλ);

    #println();
    #println(str, "  ", expfam, "  ", N, "  ", θ);
    #println("projection J $cJ  v $cv  λ $cλ");


    # check projection is idempotent
    ccv, ccλ = projection(str, expfam, N, cλ);
    @test abs(ccv) < tol;


    for J = 1:K
        for k = 1:K
            if k != J
                let (v, λ) = alt_min(str, expfam, N, θ, J, k);
                    #println("J $J  k $k  v $v  λ $λ");
                    @test abs(v - N'd.(Ref(expfam), θ, λ)) < tol; # check distance
                    @test cv ≤ v + tol; # cannot beat projection
                    @test conforms(str, λ);
                    @test λ[J] ≤ λ[k] + tol;
                    if k == cJ
                        # projection is the min in ¬J, and should be found at k=cJ
                        @test abs(cv - v) < tol;
                    end
                end
            end
        end
    end

    let J = argmax(θ)
        let (vc, λc) = alt_min_constr(str, expfam, N, θ, J)
            if vc != Inf # outside the structure :(
                @test abs(vc - N'd.(Ref(expfam), θ, λc)) < tol; # check distance
                #            @test cv ≤ vc + tol; # cannot beat projection
                @test conforms(str, λc);
                #            @test λc[J] ≤ λc[k] + tol;
                @test abs(λc[J] - θ[J]) < tol;
            end
        end
    end
end





@testset "test_structures" begin
    θ = [.1, .3, .9, .2, .7];
    N = [1, 2, 4, 8, 16];
    #println("θ ", θ);
    #println("N ", N);

    s1 = Unconstrained();
    test_structure(s1, Bernoulli(), N, θ);

    s2 = Sparse(1, .3);
    test_structure(s2, Bernoulli(), N, θ);

    s3 = Lipschitz(collect(1:length(θ)), .3);
    test_structure(s3, Gaussian(), N, θ);

    s4 = Unimodal();
    test_structure(s4, Gaussian(), N, θ);

    s5 = Simplex();
    test_structure(s5, Gaussian(), N, θ);

    s6 = Affine(collect(1:length(θ)));
    test_structure(s6, Gaussian(), N, θ);

    s7 = Concave(collect(1:length(θ)));
    test_structure(s7, Gaussian(), N, θ);

    s8 = Linear(randn(length(θ), 3));
    test_structure(s8, Gaussian(), N, θ);

    s9 = Categorized(2);
    test_structure(s9, Gaussian(), N, θ);
end


@testset "Affine is special case of Linear" begin
    tol = 1e-5;

    K = 5;
    x = collect(Float64, 1:K);
    y = [x ones(K)];

    s1 = Affine(x);
    s2 = Linear(y);

    μ = randn(K);

    for k in 1:K
        for J in 1:K
            v1, λ1 = alt_min(s1, Gaussian(), ones(K), μ, J);
            v2, λ2 = alt_min(s2, Gaussian(), ones(K), μ, J);

            @test abs(v1 - v2) < tol
            @test all(abs.(λ1 .- λ2) .< tol)
        end
    end

end
