using Test, JuMP, OSQP, MathOptInterface

include("../PAVA.jl");


# check for unimodality
function conforms(θ)
    tol = 1e-3;
    k = argmax(θ);
    all(θ[1:k-1] .≤ θ[2:k] .+ tol) &&
    all(θ[k:end-1] .≥ θ[k+1:end] .- tol)
end


# compute best unimodal function under the constraint
# that k is better than J
function slow_alt_min(N, θ, J, k)
    @assert k != J;
    model = Model(with_optimizer(OSQP.Optimizer, verbose=false, polish=true));
    K = length(θ);

    @variable(model, λ[1:K]);

    # unimodal
    if k < J
        @constraint(model, λ[1:k-2] .≤ λ[2:k-1]);       # inc on [1:k-1]
        @constraint(model, λ[k:end-1] .≥ λ[k+1:end]);   # dec on [k:end]
    else
        @constraint(model, λ[1:k-1] .≤ λ[2:k]);         # inc on [1:k]
        @constraint(model, λ[k+1:end-1] .≥ λ[k+2:end]); # dec on [k+1:end]
    end

    # weighted squared loss
    @objective(model, Min, N'*((λ .- θ).^2)/2);

    optimize!(model);
    @assert termination_status(model) == MathOptInterface.OPTIMAL;

    objective_value(model), value.(λ);
end



function slow_alt_min_constr(N, θ, J, k)
    @assert k != J;
    model = Model(with_optimizer(OSQP.Optimizer, verbose=false, polish=true));
    K = length(θ);

    @variable(model, λ[1:K]);

    # unimodal (in particular, k is best)
    @constraint(model, λ[1:k-1] .≤ λ[2:k]);
    @constraint(model, λ[k:end-1] .≥ λ[k+1:end]);

    # arm J does not move
    @constraint(model, λ[J] == θ[J]);

    # weighted squared loss
    @objective(model, Min, N'*((λ .- θ).^2)/2);

    optimize!(model);
    @assert termination_status(model) == MathOptInterface.OPTIMAL;

    objective_value(model), value.(λ);
end


K = 5;
tol = 1e-4;



@testset "prep" begin
    for it in 1:100

        N = rand(1:10, K);
        μ = randn(K);

        p,n,b,f = PAVAprep(N, μ)
        pr,nr,br,fr = PAVAprep(reverse(N), reverse(μ));

        @test all(p .== (K+1).-reverse(nr));
        @test all(n .== (K+1).-reverse(pr));
        @test all(isapprox.(b, reverse(fr), atol=tol));
        @test all(isapprox.(f, reverse(br), atol=tol));
    end
end


@testset "projection" begin
    for it in 1:100

        N = rand(1:10, K);
        μ = randn(K);

        v,λ = PAVA_project(N, μ)
        vp, λp = minimum(slow_alt_min(N, μ, 0, k) for k in 1:K);

        @test all(isapprox.(λ, λp, atol=tol));
        @test v ≈ vp atol=tol;
    end
end



@testset "alt_min" begin

    for it = 1:100
        N = rand(1:10, K);
        μ = randn(K);

        for J = 1:K
            v1, λ1 = minimum(slow_alt_min(N, μ, J, k) for k in 1:K if k != J);
            v2, λ2 = PAVA_alt_min(N, μ, J);

            @test v1 ≈ v2 atol=tol
            @test all(isapprox.(λ1, λ2, atol=tol));
        end
    end
end


@testset "alt_min_k" begin

    for it = 1:100
        N = rand(1:10, K);
        μ = randn(K);

        for J = 1:K
            for k = 1:K
                if k==J; continue; end
                v1, λ1 = slow_alt_min(N, μ, J, k);
                v2, λ2 = PAVA_alt_min(N, μ, J, k);

                @test v1 ≈ v2 atol=tol
                @test all(isapprox.(λ1, λ2, atol=tol));
            end
        end
    end
end



@testset "alt_min_constr" begin

    for it = 1:100
        N = rand(1:10, K);
        μ = randn(K);

        J = argmax(μ);

        v1, λ1 = minimum(slow_alt_min_constr(N, μ, J, k) for k in 1:K if k != J);
        v2, λ2 = PAVA_alt_min_constr(N, μ, J);

        @test v1 ≈ v2 atol=tol
        @test all(isapprox.(λ1, λ2, atol=tol));
    end
end


@testset "zeros" begin

    tol = 1e-2;

    for it = 1:100
        N = rand(0:1, K);
        μ = randn(K);

        v1, λ1 = PAVA_project(N, μ)
        v2, λ2 = minimum(slow_alt_min(N, μ, 0, k) for k in 1:K);

        @test conforms(λ1);
        #@test conforms(λ2); # disabled, optimiser gives infeasible output :(
        @test v1 ≈ v2 atol=tol
        #@test all(isapprox.(λ1, λ2, atol=tol));

    end

end
