using Printf;

include("../expfam.jl");
include("../regret.jl");
include("structures.jl");
include("lambda_learner.jl")
include("noisefree_oracle.jl");


function go()
    θ = [.1, .3, .9, .2, .7];

    ρs = range(0, 2π, length=6);
    x = [cos.(ρs) sin.(ρs)];
    ξ = randn(2);
    μ = x*ξ; # well-specified

    prs = (
        (Unconstrained(), Bernoulli(), θ),
        (Sparse(2, .3), Bernoulli(), θ),
        (Lipschitz(collect(1:length(θ)), .3), Gaussian(), θ),
        (Unimodal(), Gaussian(), θ),
        (Simplex(), Gaussian(), θ),
        (Affine(collect(1:length(θ))), Gaussian(), θ),
        (Concave(collect(1:length(θ))), Gaussian(), θ),
        (Linear(x), Gaussian(), μ),
    );

    println("------ timing k-learner ------");
    for ((str, expfam, θ)) in prs
        print(@sprintf("%-20s", typeof(str)));
        sp_perturbed_k(str, expfam, θ, 1, t->1/t) # warm-up
        @time sp_perturbed_k(str, expfam, θ, 500, t->1/t);
    end

    println("\n\n------ timing λ-learner ------");
    for ((str, expfam, θ)) in prs
        print(@sprintf("%-20s", typeof(str)));
        sp_perturbed_λ(str, expfam, θ, 1, t->1/t) # warm-up
        @time sp_perturbed_λ(str, expfam, θ, 500, t->1/t);
    end

    println("\n\n------ timing projection ------");
    for ((str, expfam, θ)) in prs
        print(@sprintf("%-20s", typeof(str)));
        projection(str, expfam, ones(length(θ)), θ) # warm-up
        @time for i in 1:1000
            projection(str, expfam, ones(length(θ)), θ)
        end
    end

    nothing
end
