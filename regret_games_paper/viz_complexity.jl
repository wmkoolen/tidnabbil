using Plots;
using JLD2;

include("runit.jl");

@load (isempty(ARGS) ? "experiment_complexity" : ARGS[1])*".dat"  str expfam μ αs T nreps algs data



# regret
q = [sum(last.(getindex.(data[:,a,b],1)))/nreps
     for b in eachindex(αs), a in eachindex(algs)
     ]
# regret stddev
q2 = [sqrt(sum((last.(getindex.(data[:,a,b],1)) .- q[b,a]).^2))/nreps
      for b in eachindex(αs), a in eachindex(algs)
      ]


# total exploration performed
n = [ sum(sum.(getfield.(getindex.(data[:,a,b],4),:n)))/nreps
      for b in eachindex(αs), a in eachindex(algs)
      ]
# exploration stddev
n2 = [ sqrt(sum((sum.(getfield.(getindex.(data[:,a,b],4),:n)) .- n[b,a]).^2))/nreps
       for b in eachindex(αs), a in eachindex(algs)
       ]


lbds = [
    begin
    w, w̃, v = sp_constrained_k(str, expfam, α*μ, 10000)
    1/v[end]*log(T)
    end
    for α in αs];

plot(αs, q, ribbon=q2, label=permutedims([long.(first.(algs))...]))
plot!(αs, lbds, label="lower bd", title="regret")

savefig("experiment_complexity_regret.pdf");

plot(αs, n, ribbon=n2, label=permutedims([long.(first.(algs))...]), title="exploration")

savefig("experiment_complexity_exploration.pdf");
