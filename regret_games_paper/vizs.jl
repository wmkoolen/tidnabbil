using Plots;
using JLD2;
using Printf;
using LinearAlgebra;


include("runit.jl");

# the idea is to either run this from a script, to get all plots, or
# to run it more targeted using something like:
#
# push!(ARGS, "experiment_categorized1")   # <- do this once
# include("vizs.jl")                       # <- repeat every re-run

names = !isempty(ARGS) ? ARGS :
    (#"experiment_concave1",
     #"experiment_affine1",
     #"experiment_simplex1",
     #"experiment_sparse1",
     #"experiment_sparse2",
     #"experiment_unconstrained1",
     #"experiment_unimodal1",
     # "experiment_linear1",
     # "experiment_categorized1",
     "experiment_categorized2",
     );


function getdesc(alg::SPk, est::Estimator)
    (color=1, name="SPk");
end

function getdesc(alg::SPλ, est::Estimator)
    (color=2, name="SPλ") # used to be "SP\\lambda"
end

function getdesc(alg::UCB, est::UnconstrainedEstimator)
    (color=3, name="UCB")
end

function getdesc(alg::UCB, est::StructuredEstimator)
    (color=4, name="M-UCB") # this stopped working in Julia 1.4 "\$\\mathcal{M}\\textrm{-UCB}\$"
end

function getdesc(alg::OSSBLike, est::Estimator)
    (color=5, name="OSSB")
end

function getdesc(alg::OSUB, est::Estimator)
    (color=6, name="OSUB")
end

function getdesc(alg::EOO, est::Estimator)
    (color=7, name="EOO")
end

function getdesc(alg::CATSE, est::Estimator)
    (color=8, name="CATSE")
end

function getdesc(alg::SparseUCB, est::Estimator)
    (color=9, name="SparseUCB")
end






for name in names

    println("\nprocessing ", name);

    print("loading ");
    @time @load name*".dat" str expfam μ T nreps algs data

    println("μ ", μ);
    println("T ", T, "  nreps ", nreps);
    for i in eachindex(algs)
        avgN = sum(map(x->x[2], data[:,i])) / nreps;
        avgn = if algs[i][1] isa UCB || algs[i][1] isa SparseUCB || algs[i][1] isa OSUB || algs[i][1] isa CATSE
            avgN # undef; do something reasonable.
        else
            sum(map(x->x[4].n, data[:,i])) / nreps;
        end

        println(@sprintf("%-20s", long(algs[i][1])),
                "   n ", join(map(x->@sprintf("%6.1f", x), avgn), " "),
                "   N ", join(map(x->@sprintf("%6.1f", x), avgN), " ")
                );
    end


    #plot(title="\\mu = [" * join(map(z->@sprintf("%.2f", z), μ), ", ") * "] in $str");
    plot(xlabel="T", ylabel="regret", legend=:topleft);

    for i in sortperm([getdesc(a...).color for a in algs])
        ER = sum(getindex.(data[:,i],1)) / nreps;
        σR = sqrt.(sum(map(x -> (x[1].-ER).^2, data[:,i]))/nreps^2);

        plot!(ER, ribbon=σR, label=getdesc(algs[i]...).name, color=getdesc(algs[i]...).color)
    end

    # rate as if we were unconstrained (the hardest case)
    Δ = maximum(μ) .- μ
    ds = d.(Ref(expfam), μ, maximum(μ));
    rate = sum(Δ[k]/ds[k] for k in eachindex(μ) if Δ[k]>0)

    # approximate real rate computation
    print("apx rate computation ");
    # @time _,_,v = sp_perturbed_k(str, expfam, μ, 10000, t->1/t);
    @time _,_,v = sp_constrained_k(str, expfam, μ, 10000);
    rate2 = 1/v[end];

    println("unconstrained rate ", rate, "  appx rate ", rate2);

    plot!([rate rate2].*log.(1:T), label=["uncstrd lbd" "lbd"], color=[:black :red], style=:dash)

    plot!(xscale=:log10);

    plot!(size=div.((600,400).*3,4));
    savefig(name*".pdf");

end
